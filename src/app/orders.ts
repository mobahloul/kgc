export  class Orders {
   id:null;
   product_id:  number;
   user_id:  number;
   price:  number;
   amount:  number;
   status:  string;
}