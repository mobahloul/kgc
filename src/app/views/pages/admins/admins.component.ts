import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { AdminsService } from '../../../service/admins.service';
import { Admins } from  '../../../admins';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-admins',
  templateUrl: './admins.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit {
	items: Admins[]=[];

	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['username','email','actions'];

	constructor(public snackBar: MatSnackBar,  public service: AdminsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.list().subscribe((data)=>{
			this.items = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
		
	}

	delete(id){
		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.service.delete(id).subscribe((data)=>{});
		}else{ return false;}

		setTimeout(() => {
			this.ngOnInit();
			}, 1000);
	}
}
