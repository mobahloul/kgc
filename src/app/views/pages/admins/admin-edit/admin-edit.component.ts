import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AdminsService } from '../../../../service/admins.service';
import { Admins } from  '../../../../admins';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'kt-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.scss']
})
export class AdminEditComponent implements OnInit {

  items: Admins[]=[];
  id=null;

  constructor(public snackBar: MatSnackBar,
    public service: AdminsService,
    public location: Location,
      public router: Router,private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    
	}

  ngOnInit() {

    this.service.get(this.id).subscribe((data)=>{
			this.items = data;
		});
  }

    onSubmit(form){
      const obj = {
        username:  form.value.username,
        email:  form.value.email,
        };

      this.service.edit(this.id,obj).subscribe((data)=>{
      });
      setTimeout(() => {
        this.cancel();
        }, 1000);
      }

      pwdUpdate(form){
        const obj = {
          password:  form.value.password,
          };
  
        this.service.pwdUpdate(this.id,obj).subscribe((data)=>{});

        setTimeout(() => {
          this.cancel();
          }, 1000);
      }

      cancel(){
        this.location.back();
        return;
			this.router.navigate(['/default/admins'], { relativeTo: this.activatedRoute });
      }
}
