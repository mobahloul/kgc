import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AdminsService } from '../../../../service/admins.service';
import { Admins } from '../../../../admins';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
	selector: 'kt-admin-create',
	templateUrl: './admin-create.component.html',
	styleUrls: ['./admin-create.component.scss']
})
export class AdminCreateComponent implements OnInit {
	Admins: Admins[] = [];
	username = null;
	password = null;
	email = null;
	password_confirm = null;
	constructor(public location: Location, public snackBar: MatSnackBar, public service: AdminsService, public router: Router, private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() {
	}

	onSubmit(form) {

		const obj = {
			username: form.value.username,
			password: form.value.password,
			email: form.value.email,
		};
		this.service.create(obj).subscribe((data) => {
			//console.log(data);
		});

		setTimeout(() => {
			this.cancel();
		}, 1000);
	}

	cancel() {
		this.location.back();
		return;
		this.router.navigate(['/default/admins'], { relativeTo: this.activatedRoute });
	}

}
