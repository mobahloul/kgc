import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreportComponent } from './treport.component';

describe('TreportComponent', () => {
  let component: TreportComponent;
  let fixture: ComponentFixture<TreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
