import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TeamsService } from '../../../../../service/teams.service';
import { Teams } from  '../../../../../teams';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-treport',
  templateUrl: './treport.component.html',
  // changeDetection: ChangeDetectionStrategy.Default,
  styleUrls: ['./treport.component.scss']
})
export class TreportComponent implements OnInit {

  id=null;
  items: Teams[]=[];
	constructor(public snackBar: MatSnackBar, private service: TeamsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

  ngOnInit(){
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    this.service.getteams(this.id).subscribe((data)=>{
      this.items = data;
    });
  }

    get_color(item){
      switch (item) {
        case 'Alpha':
          return 'primary';
          break;

          case 'Omega':
            return 'success';
          break;
      }
    }
  
}
