import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { TeamsService } from '../../../../service/teams.service';
import { Teams } from  '../../../../teams';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-teamsreport',
  templateUrl: './teamsreport.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./teamsreport.component.scss']
})
export class TeamsreportComponent implements OnInit {
	teams: Teams[]=[];
	items=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['name','teams','sub1', 'sub2', 'sub3', 'sub4','sub5','sub6','actions'];

	constructor(public snackBar: MatSnackBar, private service: TeamsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.report().subscribe((data)=>{
			this.teams = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});

	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }

	  details(id){
		this.router.navigate(['details', id], { relativeTo: this.activatedRoute });
	}

}
