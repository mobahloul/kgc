import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TeamsService } from '../../../../../service/teams.service';
import { Teams } from  '../../../../../teams';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-teams-details',
  templateUrl: './teams-details.component.html',
//  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./teams-details.component.scss']
})
export class TeamsDetailsComponent implements OnInit {

  id=null;
  items: Teams[]=[];
	constructor(public snackBar: MatSnackBar, private service: TeamsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

  ngOnInit(){
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    this.service.getteams(this.id).subscribe((data)=>{
      this.items = data;
    });
  }

    get_color(item){
      switch (item) {
        case 'Alpha':
          return 'danger';
          break;

          case 'Omega':
            return 'info';
          break;
      }
    }
  
}
