import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsreportComponent } from './teamsreport.component';

describe('TeamsreportComponent', () => {
  let component: TeamsreportComponent;
  let fixture: ComponentFixture<TeamsreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
