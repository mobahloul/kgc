import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { TeamsService } from '../../../../service/teams.service';
import { Teams } from  '../../../../teams';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-gender',
  templateUrl: './gender.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./gender.component.scss']
})
export class GenderComponent implements OnInit {
	teams: Teams[]=[];
	items=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['event_name', 'from','to','male','female'];

	constructor(public snackBar: MatSnackBar, private service: TeamsService, public router: Router,private activatedRoute: ActivatedRoute) {
		// this.stateCtrl = new FormControl();
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.genderReport().subscribe((data)=>{
			this.teams = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});

	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }

}
