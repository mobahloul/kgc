import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BookingService } from '../../../../service/booking.service';
import { Booking } from  '../../../../booking';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-payment',
  templateUrl: './payment.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

	items: Booking[]=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['event_name', 'from','to','cash','online','refunded'];

	constructor(private service: BookingService, public router: Router,private activatedRoute: ActivatedRoute) {
	}


	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.paymentReport().subscribe((data)=>{
			this.items = data;
			this.dataSource.data = data;
			this.resultsLength = data.length;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }

}
