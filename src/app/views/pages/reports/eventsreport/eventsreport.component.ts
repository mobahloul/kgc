import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { EventService } from '../../../../service/event.service';
import { events } from  '../../../../events';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-eventsreport',
  templateUrl: './eventsreport.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./eventsreport.component.scss']
})
export class EventsreportComponent implements OnInit {
	events: events[]=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['name', 'startDate', 'endDate', 'boys','girls','fees','total_alpha','total_omega'];

	constructor(public snackBar: MatSnackBar,  public evnt: EventService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
	this.get_all();
	}

	get_all(){
		this.evnt.report().subscribe((data)=>{
			this.events = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }

}