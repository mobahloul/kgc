import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsreportComponent } from './eventsreport.component';

describe('EventsreportComponent', () => {
  let component: EventsreportComponent;
  let fixture: ComponentFixture<EventsreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
