import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BookingService } from '../../../service/booking.service';
import { Booking } from  '../../../booking';
import { EventService } from '../../../service/event.service';
import { events } from  '../../../events';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'kt-familybooking',
  templateUrl: './familybooking.component.html',
  styleUrls: ['./familybooking.component.scss']
})
export class FamilybookingComponent implements OnInit {
	items: Booking[]=[];
	events: events[]=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['firstName', 'event_name', 'approval',  'paymentAvailability', 'paymentStatus', 'paymentMethod','created_at','actions'];

	//   status = ['Finished','Waiting List','Refund'];
	  payment = ['Unpaid','Paid','Waiting Paid','Waiting Unpaid','canceled','refunded'];
	  method = ['Cash','Online'];

	constructor(private service: BookingService,public evnt: EventService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.listFamily().subscribe((data)=>{
			this.items = data;
			this.dataSource.data = data;
			this.resultsLength = data.length;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
		
		getItemCssClass(team: string): string {
			switch (team) {
				case 'Waiting List':
					return 'danger';
				case 'Reservation Done':
					return 'success';
				case 'Refunded':
					return 'danger';
				case 'Yes':
					return 'success';
				case 'Approved':
						return 'success';
				
			}
			return '';
		}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
	}

	check(id){
		this.router.navigate(['/default/guardians/edit',id], { relativeTo: this.activatedRoute });
	}

	delete(id){

		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.service.delete(id).subscribe((data)=>{});
		}else{ return false;}
		
		setTimeout(() => {
		this.router.navigate(['/default/familybooking'], { relativeTo: this.activatedRoute });
			}, 1000);
	}
}
