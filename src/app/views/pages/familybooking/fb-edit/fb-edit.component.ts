import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { BookingService } from '../../../../service/booking.service';
import { Booking } from '../../../../booking';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'kt-fb-edit',
  templateUrl: './fb-edit.component.html',
  styleUrls: ['./fb-edit.component.scss']
})
export class FbEditComponent implements OnInit {

  payments = ['Unpaid', 'Paid', 'Waiting Paid', 'Waiting Unpaid', 'canceled', 'refunded'];
  approves = ['Waiting', 'Approved', 'Declined'];

  options = ['Yes', 'No'];
  teams = ['Alpha', 'Omega'];

  options2 = [
    { key: 'Cash', value: "Cash" },
    { key: 'Online', value: "Online" }
  ];

  items: Booking[] = [];
  id = null;

  constructor(public location: Location, public snackBar: MatSnackBar, private service: BookingService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.service.getFamily(this.id).subscribe((data) => {
      this.items = data;
    });
  }

  onSubmit(form) {

    const obj = {
      paymentStatus: form.value.paymentStatus,
      team: form.value.team,
      approval: form.value.approval,
      paymentAvailability: form.value.paymentAvailability,
      paymentMethod: form.value.paymentMethod
    };

    this.service.editFamily(this.id, obj).subscribe((data) => { });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/family-booking'], { relativeTo: this.activatedRoute });
  }
}
