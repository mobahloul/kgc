import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilybookingComponent } from './familybooking.component';

describe('FamilybookingComponent', () => {
  let component: FamilybookingComponent;
  let fixture: ComponentFixture<FamilybookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilybookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilybookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
