// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
// NgBootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { MailModule } from './apps/mail/mail.module';
import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
import { UserManagementModule } from './user-management/user-management.module';
import { CoreModule } from '../../core/core.module';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CodePreviewModule } from '../partials/content/general/code-preview/code-preview.module';
import { MaterialPreviewModule } from '../partials/content/general/material-preview/material-preview.module';


import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconRegistry } from '@angular/material/icon';
import {
	MatAutocompleteModule,
	MatNativeDateModule,
	MatFormFieldModule,
	MatInputModule,
	MatRadioModule,
	MatButtonModule,
	MatCardModule,
	MatChipsModule,
	MatSelectModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatIconModule,
	MatSliderModule,
	MatPaginatorModule,
	MatSortModule,
	MatSidenavModule,
	MatSnackBarModule,
	MatStepperModule,
	MatToolbarModule,
	MatDividerModule,
	MatTabsModule,
	MatTableModule,
	MatTooltipModule,
	MatListModule,
	MatGridListModule,
	MatButtonToggleModule,
	MatBottomSheetModule,
	MatExpansionModule,
	_MatChipListMixinBase,
	MatMenuModule,
	MatTreeModule,
	MAT_BOTTOM_SHEET_DATA,
	MatBottomSheetRef,
	MAT_DATE_LOCALE,
	MAT_DATE_FORMATS,

} from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { newsStatus } from '../../pipes/newsStatus';
import { PostDate } from '../../pipes/PostDate';
import { Intpipe } from '../../pipes/intpipe';

// Form controls
import { MatDatepickerModule, MatDatepickerIntl } from '@angular/material/datepicker';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';

import { EventsComponent } from './events/events.component';
import { CreateComponent } from './events/create/create.component';
import { EditComponent } from './events/edit/edit.component';

import { GurdiansComponent } from './gurdians/gurdians.component';
import { GuardEditComponent } from './gurdians/guard-edit/guard-edit.component';

import { MeetingPointsComponent } from './meeting-points/meeting-points.component';
import { PointCreateComponent } from './meeting-points/point-create/point-create.component';
import { PointEditComponent } from './meeting-points/point-edit/point-edit.component';

import { KidsComponent } from './kids/kids.component';
import { KidEditComponent } from './kids/kid-edit/kid-edit.component';

import { AdminsComponent } from './admins/admins.component';
import { AdminCreateComponent } from './admins/admin-create/admin-create.component';
import { AdminEditComponent } from './admins/admin-edit/admin-edit.component';

import { BookingComponent } from './booking/booking.component';
import { BookingEditComponent } from './booking/booking-edit/booking-edit.component';

import { TeamsComponent } from './teams/teams.component';
import { ReservationComponent } from './teams/reservation/reservation.component';
//reports
import { EventsreportComponent } from './reports/eventsreport/eventsreport.component';
import { TeamsreportComponent } from './reports/teamsreport/teamsreport.component';
import { TeamsDetailsComponent } from './reports/teamsreport/teams-details/teams-details.component';
import { TreportComponent } from './reports/teamsreport/treport/treport.component';

import { CabinsReportsComponent } from './cabins-reports/cabins-reports.component';
import { CabinDetailsComponent } from './cabins-reports/cabin-details/cabin-details.component';
import { HomeComponent } from './home/home.component';
import { PaymentComponent } from './reports/payment/payment.component';
import { BookingreportComponent } from './reports/bookingreport/bookingreport.component';
import { GenderComponent } from './reports/gender/gender.component';
import { SportsComponent } from './sports/sports.component';
import { SportCreateComponent } from './sports/sport-create/sport-create.component';
import { SportEditComponent } from './sports/sport-edit/sport-edit.component';
import { StoreComponent } from './store/store.component';
import { StoreCreateComponent } from './store/store-create/store-create.component';
import { StoreEditComponent } from './store/store-edit/store-edit.component';
import { GuardCreateComponent } from './gurdians/guard-create/guard-create.component';
import { GuardViewComponent } from './gurdians/guard-view/guard-view.component';
import { FamilyeventsComponent } from './familyevents/familyevents.component';
import { FeAddComponent } from './familyevents/fe-add/fe-add.component';
import { FeEditComponent } from './familyevents/fe-edit/fe-edit.component';
import { FamilybookingComponent } from './familybooking/familybooking.component';
import { FbEditComponent } from './familybooking/fb-edit/fb-edit.component';
import { OrdersComponent } from './orders/orders.component';
import { OtherguardianComponent } from './otherguardian/otherguardian.component';
import { OgCreateComponent } from './otherguardian/og-create/og-create.component';
import { OgEditComponent } from './otherguardian/og-edit/og-edit.component';
import { OgViewComponent } from './otherguardian/og-view/og-view.component';
import { KidCreateComponent } from './kids/kid-create/kid-create.component';
import { KidViewComponent } from './kids/kid-view/kid-view.component';
import { BookingCreateComponent } from './booking/booking-create/booking-create.component';
import { OrdersCreateComponent } from './orders/orders-create/orders-create.component';
import { OrdersViewComponent } from './orders/orders-view/orders-view.component';
import { OrdersEditComponent } from './orders/orders-edit/orders-edit.component';
import { GalleriesComponent } from './galleries/galleries.component';
import { AddGalleryComponent } from './galleries/add-gallery/add-gallery.component';
import { EditGalleryComponent } from './galleries/edit-gallery/edit-gallery.component';
import { NewsComponent } from './news/news.component';
import { AddNewsComponent } from './news/add-news/add-news.component';
import { EditNewsComponent } from './news/edit-news/edit-news.component';
import { CouponsComponent } from './coupons/coupons.component';
import { CouponseditComponent } from './coupons/coupons-edit/coupon-edit.component';
import { CouponsCreateComponent } from './coupons/coupons-create/coupons-create.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { AlbumsComponent } from './albums/albums.component';
import { AddAlbumComponent } from './albums/add-album/add-album.component';
import { EditAlbumComponent } from './albums/edit-album/edit-album.component';
@NgModule({
	declarations: [ EventsComponent, CreateComponent, EditComponent, GurdiansComponent, GuardEditComponent, MeetingPointsComponent, PointCreateComponent, PointEditComponent, KidsComponent, KidEditComponent, AdminsComponent, AdminCreateComponent, AdminEditComponent, BookingComponent, BookingEditComponent, TeamsComponent, ReservationComponent, EventsreportComponent, TeamsreportComponent, TeamsDetailsComponent, TreportComponent, CabinsReportsComponent, CabinDetailsComponent, HomeComponent, PaymentComponent, BookingreportComponent, GenderComponent, SportsComponent, SportCreateComponent, SportEditComponent, StoreComponent, StoreCreateComponent, StoreEditComponent, GuardCreateComponent, GuardViewComponent, FamilyeventsComponent, FeAddComponent, FeEditComponent, FamilybookingComponent, FbEditComponent, OrdersComponent, OtherguardianComponent, OgCreateComponent, OgEditComponent, OgViewComponent, KidCreateComponent, KidViewComponent, BookingCreateComponent, OrdersCreateComponent, OrdersViewComponent, OrdersEditComponent, GalleriesComponent, AddGalleryComponent, EditGalleryComponent, NewsComponent, AddNewsComponent, EditNewsComponent,CouponsComponent,CouponsCreateComponent,CouponseditComponent,newsStatus,PostDate,AlbumsComponent,AddAlbumComponent,EditAlbumComponent,Intpipe],
	exports: [RouterModule],
	imports: [
CKEditorModule,
		CommonModule,
		HttpClientModule,
		FormsModule,
		NgbModule,
		CoreModule,
		PartialsModule,
		MailModule,
		ECommerceModule,
		UserManagementModule,
				// material modules
				MatInputModule,
				MatFormFieldModule,
				MatDatepickerModule,
				MatAutocompleteModule,
				MatListModule,
				MatSliderModule,
				MatCardModule,
				MatSelectModule,
				MatButtonModule,
				MatIconModule,
				MatNativeDateModule,
				MatSlideToggleModule,
				MatCheckboxModule,
				MatMenuModule,
				MatTabsModule,
				MatTooltipModule,
				MatSidenavModule,
				MatProgressBarModule,
				MatProgressSpinnerModule,
				MatSnackBarModule,
				MatTableModule,
				MatGridListModule,
				MatToolbarModule,
				MatBottomSheetModule,
				MatExpansionModule,
				MatDividerModule,
				MatSortModule,
				MatStepperModule,
				MatChipsModule,
				MatPaginatorModule,
				MatDialogModule,
				MatRippleModule,
				CoreModule,
				CommonModule,
				MatRadioModule,
				MatTreeModule,
				MatButtonToggleModule,
				PartialsModule,
				MaterialPreviewModule,
				FormsModule,
				ReactiveFormsModule,
				CodePreviewModule
	],

	providers: [
		MatIconRegistry,
		{ provide: MatBottomSheetRef, useValue: {} },
		{ provide: MAT_BOTTOM_SHEET_DATA, useValue: {} },
		{ provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
		{ provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },

	],

})
export class PagesModule {
}
