import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { StoreService } from '../../../service/store.service';
import { OrdersService } from '../../../service/orders.service';
import { Store } from  '../../../store';
import { Orders } from  '../../../orders';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

	Store: Store[]=[];
	Orders: Orders[]=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
  displayedColumns1: string[] = ['user','productName','price','amount','status','actions'];
  
	constructor(public snackBar: MatSnackBar, public store: StoreService, public service: OrdersService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.list().subscribe((data)=>{
			this.Orders = data;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
		
	}

  view(id){
		this.router.navigate(['view', id], { relativeTo: this.activatedRoute });
		
  }
  
	delete(id){
		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.service.delete(id).subscribe((data)=>{});
        }else{ return false;}
		
		setTimeout(() => {
			this.ngOnInit();
        }, 1000);
		
	}
}