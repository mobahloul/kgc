import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { StoreService } from '../../../../service/store.service';
import { OrdersService } from '../../../../service/orders.service';
import { Store } from '../../../../store';
import { Orders } from '../../../../orders';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'kt-orders-view',
  templateUrl: './orders-view.component.html',
  styleUrls: ['./orders-view.component.scss']
})
export class OrdersViewComponent implements OnInit {

  products: Store[] = [];
  items: Orders[] = [];
  id = null;

  constructor(public location: Location, public snackBar: MatSnackBar, public store: StoreService, public service: OrdersService, public router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.store.list().subscribe((data) => {
      this.products = data;
    });

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.service.get(this.id).subscribe((data) => {
      this.items = data;
    });

  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/orders'], { relativeTo: this.activatedRoute });
  }

  edit(id) {
    this.router.navigate(['/default/orders/edit', id], { relativeTo: this.activatedRoute });
  }

}
