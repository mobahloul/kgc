import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { StoreService } from '../../../../service/store.service';
import { OrdersService } from '../../../../service/orders.service';
import { Store } from '../../../../store';
import { Orders } from '../../../../orders';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'kt-orders-edit',
  templateUrl: './orders-edit.component.html',
  styleUrls: ['./orders-edit.component.scss']
})
export class OrdersEditComponent implements OnInit {

  products: Store[] = [];
  items: Orders[] = [];
  id = null;

  constructor(public location: Location, public snackBar: MatSnackBar, public store: StoreService, public service: OrdersService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.store.list().subscribe((data) => {
      this.products = data;
    });

    this.service.get(this.id).subscribe((data) => {
      this.items = data;
    });

  }

  onSubmit(form) {

    const obj = {
      product_id: form.value.product_id,
      price: form.value.price,
      amount: form.value.amount,
      status: form.value.status,
    };
    this.service.edit(this.id, obj).subscribe((data) => {
    });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/orders'], { relativeTo: this.activatedRoute });
  }
}
