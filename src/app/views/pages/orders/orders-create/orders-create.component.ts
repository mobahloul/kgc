import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { StoreService } from '../../../../service/store.service';
import { OrdersService } from '../../../../service/orders.service';
import { Store } from '../../../../store';
import { Orders } from '../../../../orders';
import { ActivatedRoute, Router } from '@angular/router';

import { GuardiansService } from '../../../../service/guardians.service';
import { Guardians } from '../../../../guardians';
import { Location } from '@angular/common';

@Component({
	selector: 'kt-orders-create',
	templateUrl: './orders-create.component.html',
	styleUrls: ['./orders-create.component.scss']
})
export class OrdersCreateComponent implements OnInit {

	products: Store[] = [];
	Orders: Orders[] = [];
	Guardians: Guardians[] = [];
	amount: number;
	price: number;
	status: number;
	user_id: number;
	product_id: number;
	statusOptions = [
		{ key: 'Active', value: "Active" },
		{ key: 'Inactive', value: "Inactive" }
	];

	constructor(public snackBar: MatSnackBar,
		public location: Location,
		public store: StoreService,
		private GuardiansService: GuardiansService,
		public service: OrdersService,
		public router: Router,
		private activatedRoute: ActivatedRoute) { }

	ngOnInit() {

		this.store.list().subscribe((data) => {
			this.products = data;
		});

		this.GuardiansService.list().subscribe((data2) => {
			this.Guardians = data2;
		});
	}

	onSubmit(form) {

		const obj = {
			user_id: form.value.user_id,
			productName: form.value.productName,
			price: form.value.price,
			amount: form.value.amount,
			status: form.value.status,
		};
		this.service.create(obj).subscribe((data) => {
		});

		setTimeout(() => {
			this.cancel();
		}, 1000);
	}

	cancel() {
		this.location.back();
		return;
		this.router.navigate(['/default/store'], { relativeTo: this.activatedRoute });
	}
}
