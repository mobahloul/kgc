import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { events } from '../../../../events';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms';

import { EventService } from '../../../../service/event.service';
import { SportsService } from '../../../../service/sports.service';
import { MeetingpointsService } from '../../../../service/meetingpoints.service';
import { EventsMeetingpointsService } from '../../../../service/events-meetingpoints.service';
import { EventsSportsService } from '../../../../service/events-sports.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class EditComponent implements OnInit {
  addForm: FormGroup;
  addForm2: FormGroup;
  rows: FormArray;
  rows2: FormArray;
  itemForm: FormGroup;
  singleRooms: boolean = false;
  doubleRooms: boolean = false;
  doubleRoomsPrice = 0;
  singleRoomsPrice = 0;
  types = [
    { key: 'KGC1', value: "KGC1" },
    { key: 'KGC2', value: "KGC2" },
    { key: 'KGC3', value: "KGC3" },
    { key: 'KGC4', value: "KGC4" },
    { key: 'Christmas', value: "Christmas" },
    { key: 'Family', value: "Family" },
  ];

  genders = [
    { key: 'Male', value: "Male" },
    { key: 'Female', value: "Female" },
    { key: 'Both', value: "Both" },
  ];

  items: events[] = [];
  id = null;
  type: any;
  sports: any;
  points: any;

  constructor(public snackBar: MatSnackBar, private fb: FormBuilder, public SportsService: SportsService,
    public point: MeetingpointsService, public evnt: EventService, public eventPoints: EventsMeetingpointsService,
    public eventssports: EventsSportsService, private location: Location, public router: Router, private activatedRoute: ActivatedRoute) {
    this.addForm = this.fb.group({
      items: [null, Validators.required],
    });
    this.rows = this.fb.array([]);

    this.addForm2 = this.fb.group({
      items: [null, Validators.required],
    });
    this.rows2 = this.fb.array([]);

    this.id = this.activatedRoute.snapshot.paramMap.get('id');

  }

  ngOnInit() {
    this.evnt.get(this.id).subscribe((data) => {
      this.items = data;
      // console.log(this.items)
      this.type = this.items[0]["type"];
      this.singleRooms = this.items[0]["singleRoomsPrice"] > 0 ? true : false;
      this.doubleRooms = this.items[0]["doubleRoomsPrice"] > 0 ? true : false;
      this.items[0]["toThree"] = this.items[0]["toThree"] == 1 ? true : false;
      this.items[0]["toSix"] = this.items[0]["toSix"] == 1 ? true : false;
      this.items[0]["plusSix"] = this.items[0]["plusSix"] == 1 ? true : false;


    });

    this.addForm.addControl('rows', this.rows);
    this.addForm2.addControl('rows2', this.rows2);

    this.SportsService.list().subscribe((data) => {
      this.sports = data;
    });

    this.point.list().subscribe((data) => {
      this.points = data;
    });

    this.eventPoints.get(this.id).subscribe((res) => {
      for (let mp of res) {
        this.rows.push(this.getItemFormGroup(mp));
      }
    });

    this.eventssports.get(this.id).subscribe((results) => {
      for (let sp of results) {
        this.rows2.push(this.getSportFormGroup(sp));
      }
    });
    // console.log(this.items)

  }

  selectChange(eventType) {
    // console.log(eventType)
    this.type = eventType
  }

  onSubmit(form) {
    // // console.log(this.items)
    // // console.log(form)

    const obj = {
      name: form.value.name,
      description: form.value.description,
      mapLink: form.value.mapLink,
      startDate: form.value.startDate,
      endDate: form.value.endDate,
      ageFrom: form.value.ageFrom,
      ageTo: form.value.ageTo,
      publishDate: form.value.publishDate,
      openingDate: form.value.openingDate,
      closingDate: form.value.closingDate,
      boys: form.value.boys,
      girls: form.value.girls,
      boysCabins: form.value.boysCabins,
      girlsCabins: form.value.girlsCabins,
      fees: form.value.fees,
      eventlocation: form.value.eventlocation,
      waitingListBoys: form.value.waitingListBoys,
      waitingListGirls: form.value.waitingListGirls,
      type: form.value.type,
      toThree: this.items[0]["toThree"] && this.items[0]["toThree"] == true ? 1 : 0,
      toSix: this.items[0]["toSix"] && this.items[0]["toSix"] == true ? 1 : 0,
      plusSix: this.items[0]["plusSix"] && this.items[0]["plusSix"] == true ? 1 : 0,
      MaleCabins: form.value.MaleCabins,
      FemaleCabins: form.value.FemaleCabins,
      cabinBeds: form.value.cabinBeds,
      hotelRooms: form.value.hotelRooms,
      hotelBeds: form.value.hotelBeds,
      child3Fees: form.value.child3Fees,
      child6Fees: form.value.child6Fees,
      busFees: form.value.busFees,
      director: form.value.director,
      coDirector: form.value.coDirector,
      staffHeadBoys: form.value.staffHeadBoys,
      staffHeadGirls: form.value.staffHeadGirls,
      adminHead: form.value.adminHead,
      campDoctor: form.value.campDoctor,
      photographer: form.value.photographer,
      soundMan: form.value.soundMan,
      adminAssistant: form.value.adminAssistant,
      sportsHead: form.value.sportsHead,
      sportsAssistant: form.value.sportsAssistant,
      campCoach: form.value.campCoach,
      igniteHead: form.value.igniteHead,
      micMan: form.value.micMan,
      specializedStaff: form.value.specializedStaff,
      themeDesigner: form.value.themeDesigner,
      contactNumber: form.value.contactNumber,
      campPhone: form.value.campPhone,
      blacklist: form.value.blacklist,
      medicalCases: form.value.medicalCases,
      winnerTeam: form.value.winnerTeam,
      majorIssues: form.value.majorIssues,
      comments: form.value.comments,
      singleRoomsPrice: this.items[0]["singleRoomsPrice"],
      doubleRoomsPrice: this.items[0]["doubleRoomsPrice"]
    };

    // console.log("Saving Data :", obj)
    this.evnt.edit(this.id, obj).subscribe(data => {
      if (data['result'] == 'Done') {
        // console.log(data);
        let meetingPoints = this.rows.value;
        let sports = this.rows2.value;

        this.eventPoints.delete(this.id).subscribe((data) => {
          for (let mp of meetingPoints) {
            // let time = mp.departure_time.hour+':'+mp.departure_time.minute;
            let time = mp.departure_time;
            this.eventPoints.create(this.id, mp.point_id, time).subscribe((data) => { });
          }
        });
        this.eventssports.delete(this.id).subscribe((data) => {
          for (let sp of sports) {
            this.eventssports.create(sp, this.id).subscribe((data) => { });
          }
        });

      }

    });
    setTimeout(() => {
      // alert("ffff")
      this.cancel();
    }, 500);
  }

  cancel() {
    this.location.back();
    // alert("fdfd")
    return;
  }

  //
  onAddRow() {
    this.rows.push(this.createItemFormGroup());
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      point_id: null,
      departure_time: null,
    });
  }


  getItemFormGroup(data): FormGroup {
    return this.fb.group({
      point_id: data.point_id,
      departure_time: data.departure_time,
    });
  }


  onRemoveRow(rowIndex: number) {
    this.rows.removeAt(rowIndex);
  }

  //
  onAddSport() {
    this.rows2.push(this.createSportFormGroup());
  }

  createSportFormGroup(): FormGroup {
    return this.fb.group({
      sport_id: null,
      gender: null,
      seats: null,
      ageFrom: null,
      ageTo: null,
    });
  }

  getSportFormGroup(data): FormGroup {
    return this.fb.group({
      sport_id: data.sport_id,
      gender: data.gender,
      seats: data.seats,
      ageFrom: data.ageFrom,
      ageTo: data.ageTo,
    });
  }

  onRemoveSport(rowIndex: number) {
    this.rows2.removeAt(rowIndex);
  }

}
