import { Component, OnInit, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { events } from '../../../../events';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms';

import { EventService } from '../../../../service/event.service';
import { SportsService } from '../../../../service/sports.service';
import { MeetingpointsService } from '../../../../service/meetingpoints.service';
import { EventsMeetingpointsService } from '../../../../service/events-meetingpoints.service';
import { EventsSportsService } from '../../../../service/events-sports.service';

export const MY_FORMATS = {
	parse: {
		dateInput: 'LL',
	},
	display: {
		dateInput: 'DD-MMM-YYYY',
		monthYearLabel: 'YYYY',
		dateA11yLabel: 'LL',
		monthYearA11yLabel: 'YYYY',
	},
};

@Component({
	selector: 'kt-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss'],
	providers: [
		{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
		{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
	],
})

export class CreateComponent implements OnInit {
	addForm: FormGroup;
	addForm2: FormGroup;
	rows: FormArray;
	rows2: FormArray;
	itemForm: FormGroup;
	submitted: number = 0;
	toThree: boolean = false;
	toSix: boolean = false;
	plusSix: boolean = false;
	singleRooms: boolean = false;
	doubleRooms: boolean = false;
	doubleRoomsPrice = 0;
	singleRoomsPrice = 0;
	eventType: any;
	type: any;
	sports: any;

	types = [
		{ key: 'KGC1', value: "KGC1" },
		{ key: 'KGC2', value: "KGC2" },
		{ key: 'KGC3', value: "KGC3" },
		{ key: 'KGC4', value: "KGC4" },
		{ key: 'Christmas', value: "Christmas" },
		{ key: 'Family', value: "Family" },
	];

	genders = [
		{ key: 'Male', value: "Male" },
		{ key: 'Female', value: "Female" },
		{ key: 'Both', value: "Both" },
	];

	boys: number = 120;
	girls: number = 120;
	boysCabins: number = 10;
	girlsCabins: number = 10;
	waitingListBoys: number = 0;
	waitingListGirls: number = 0;
	points: Array<any> = [];

	events: events[] = [];

	name = null;
	description = null;
	mapLink = null;
	startDate = null;
	endDate = null;
	ageFrom = null;
	ageTo = null;
	publishDate = null;
	openingDate = null;
	closingDate = null;

	fees = null;
	eventlocation = null;



	MaleCabins = null;
	FemaleCabins = null;
	cabinBeds = null;
	hotelRooms = null;
	hotelBeds = null;
	child3Fees = null;
	child6Fees = null;
	busFees = null;
	director = null;
	coDirector = null;
	staffHeadBoys = null;
	staffHeadGirls = null;
	adminHead = null;
	campDoctor = null;
	photographer = null;
	soundMan = null;
	adminAssistant = null;
	sportsHead = null;
	sportsAssistant = null;
	campCoach = null;
	igniteHead = null;
	micMan = null;
	specializedStaff = null;
	themeDesigner = null;
	contactNumber = null;
	campPhone = null;
	blacklist = null;
	medicalCases = null;
	winnerTeam = null;
	majorIssues = null;
	comments = null;
	campTheme = null;

	constructor(public snackBar: MatSnackBar,
		private fb: FormBuilder,
		public SportsService: SportsService,
		public point: MeetingpointsService,
		public evnt: EventService,
		public meetingpoints: EventsMeetingpointsService,
		public eventssports: EventsSportsService,
		public router: Router,
		private location: Location,
		private activatedRoute: ActivatedRoute) {

		this.addForm = this.fb.group({
			items: [null, Validators.required],
		});
		this.rows = this.fb.array([]);

		this.addForm2 = this.fb.group({
			items: [null, Validators.required],
		});
		this.rows2 = this.fb.array([]);
	}

	ngOnInit() {
		this.SportsService.list().subscribe((data) => {
			this.sports = data;
		});

		this.point.list().subscribe((data) => {
			this.points = data;
		});
		this.addForm.addControl('rows', this.rows);
		this.addForm2.addControl('rows2', this.rows2);

	}

	selectChange() {
		// this.type = (this.eventType == "Family") ? "Family" : "";
	}

	onSubmit(form) {
		this.submitted = 1;
		const obj = {
			name: form.value.name,
			description: form.value.description,
			mapLink: form.value.mapLink,
			startDate: form.value.startDate,
			endDate: form.value.endDate,
			ageFrom: form.value.ageFrom,
			ageTo: form.value.ageTo,
			publishDate: form.value.publishDate,
			openingDate: form.value.openingDate,
			closingDate: form.value.closingDate,
			boys: form.value.boys,
			girls: form.value.girls,
			boysCabins: form.value.boysCabins,
			girlsCabins: form.value.girlsCabins,
			fees: form.value.fees,
			eventlocation: form.value.eventlocation,
			waitingListBoys: form.value.waitingListBoys,
			waitingListGirls: form.value.waitingListGirls,
			type: form.value.type,
			toThree: this.toThree && this.toThree == true ? 1 : 0,
			toSix: this.toSix && this.toSix == true ? 1 : 0,
			plusSix: this.plusSix && this.plusSix == true ? 1 : 0,
			MaleCabins: form.value.MaleCabins,
			FemaleCabins: form.value.FemaleCabins,
			cabinBeds: form.value.cabinBeds,
			hotelRooms: form.value.hotelRooms,
			hotelBeds: form.value.hotelBeds,
			child3Fees: form.value.child3Fees,
			child6Fees: form.value.child6Fees,
			busFees: form.value.busFees,
			director: form.value.director,
			coDirector: form.value.coDirector,
			staffHeadBoys: form.value.staffHeadBoys,
			staffHeadGirls: form.value.staffHeadGirls,
			adminHead: form.value.adminHead,
			campDoctor: form.value.campDoctor,
			photographer: form.value.photographer,
			soundMan: form.value.soundMan,
			adminAssistant: form.value.adminAssistant,
			sportsHead: form.value.sportsHead,
			sportsAssistant: form.value.sportsAssistant,
			campCoach: form.value.campCoach,
			igniteHead: form.value.igniteHead,
			micMan: form.value.micMan,
			specializedStaff: form.value.specializedStaff,
			themeDesigner: form.value.themeDesigner,
			contactNumber: form.value.contactNumber,
			campPhone: form.value.campPhone,
			blacklist: form.value.blacklist,
			medicalCases: form.value.medicalCases,
			winnerTeam: form.value.winnerTeam,
			majorIssues: form.value.majorIssues,
			comments: form.value.comments,
			singleRoomsPrice: this.singleRooms ? this.singleRoomsPrice : 0,
			doubleRoomsPrice: this.singleRooms ? this.doubleRoomsPrice : 0

		};

		// console.log(obj);
		this.evnt.create(obj).subscribe(data => {
			if (data['result'] == 'Done') {
				let meetingPoints = this.rows.value;
				let sports = this.rows2.value;

				let event_id = data['id'];

				for (let mp of meetingPoints) {
					// let time = mp.departure_time.hour+':'+mp.departure_time.minute;
					let time = mp.departure_time;
					this.meetingpoints.create(event_id, mp.point_id, time).subscribe((data) => { // console.log(data);
					 });
				}

				for (let sp of sports) {
					this.eventssports.create(sp, event_id).subscribe((data) => { });
				}
			}

		});
		setTimeout(() => {
			this.cancel();
		}, 1000);
	}

	cancel() {
		this.location.back();
		return;
		// this.location.back();
		this.router.navigate(['/default/events'], { relativeTo: this.activatedRoute });
	}

	//
	onAddRow() {
		this.rows.push(this.createItemFormGroup());
	}

	onRemoveRow(rowIndex: number) {
		this.rows.removeAt(rowIndex);
	}

	createItemFormGroup(): FormGroup {
		return this.fb.group({
			point_id: null,
			departure_time: null,
		});
	}

	//
	onAddSport() {
		this.rows2.push(this.createSportFormGroup());
	}

	onRemoveSport(rowIndex: number) {
		this.rows2.removeAt(rowIndex);
	}

	createSportFormGroup(): FormGroup {
		return this.fb.group({
			sport_id: null,
			gender: null,
			seats: null,
			ageFrom: null,
			ageTo: null,
		});
	}

}
