import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyeventsComponent } from './familyevents.component';

describe('FamilyeventsComponent', () => {
  let component: FamilyeventsComponent;
  let fixture: ComponentFixture<FamilyeventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyeventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyeventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
