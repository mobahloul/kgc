import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { EventService } from '../../../../service/event.service';
import { ActivatedRoute, Router } from '@angular/router';
import { events } from '../../../../events';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-fe-edit',
  templateUrl: './fe-edit.component.html',
  styleUrls: ['./fe-edit.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class FeEditComponent implements OnInit {

  events: events[] = [];
  id = null;

  constructor(public location: Location, public snackBar: MatSnackBar, public evnt: EventService, public router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.evnt.get(this.id).subscribe((data) => {
      this.events = data;
      // console.log(this.events);
    });
  }

  onSubmit(form) {

    const obj = {
      name: form.value.name,
      description: form.value.description,
      startDate: form.value.startDate,
      endDate: form.value.endDate,
      publishDate: form.value.publishDate,
      openingDate: form.value.openingDate,
      closingDate: form.value.closingDate,
      location: form.value.location,
      MaleCabins: form.value.MaleCabins,
      FemaleCabins: form.value.FemaleCabins,
      cabinBeds: form.value.cabinBeds,
      hotelRooms: form.value.hotelRooms,
      hotelBeds: form.value.hotelBeds,
      guardianFees: form.value.guardianFees,
      child3Fees: form.value.child3Fees,
      child6Fees: form.value.child6Fees,
      busFees: form.value.busFees
    };

    this.id = form.value.id;

    // console.log(obj);
    this.evnt.editFamily(this.id, obj).subscribe((data) => {
      // console.log(data);
    });

    this.cancel();
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/family-events'], { relativeTo: this.activatedRoute });
  }
}
