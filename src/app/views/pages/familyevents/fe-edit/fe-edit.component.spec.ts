import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeEditComponent } from './fe-edit.component';

describe('FeEditComponent', () => {
  let component: FeEditComponent;
  let fixture: ComponentFixture<FeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
