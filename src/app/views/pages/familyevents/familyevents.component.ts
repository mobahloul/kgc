import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { EventService } from '../../../service/event.service';
import { events } from  '../../../events';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-familyevents',
  templateUrl: './familyevents.component.html',
  styleUrls: ['./familyevents.component.scss']
})
export class FamilyeventsComponent implements OnInit {
  stateCtrl: FormControl;
	events: events[]=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['name', 'startDate', 'endDate','fees','actions'];

	constructor(public snackBar: MatSnackBar, private cd: ChangeDetectorRef, public evnt: EventService, public router: Router,private activatedRoute: ActivatedRoute) {
		this.stateCtrl = new FormControl();
	}

	ngOnInit() { 
		this.cd.detectChanges();
		this.get_all();
	}

	get_all(){
		this.evnt.listFamily().subscribe((data)=>{
			this.events = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			this.cd.detectChanges();

		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
	}

	delete(id){
		this.evnt.delete(id).subscribe((data)=>{
			// console.log(data);
		});
		window.location.href=this.router.url;
		
	}

}