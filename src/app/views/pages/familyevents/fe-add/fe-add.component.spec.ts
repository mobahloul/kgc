import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeAddComponent } from './fe-add.component';

describe('FeAddComponent', () => {
  let component: FeAddComponent;
  let fixture: ComponentFixture<FeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
