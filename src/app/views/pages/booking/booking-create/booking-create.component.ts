import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { BookingService } from '../../../../service/booking.service';
import { Booking } from '../../../../booking';
import { ActivatedRoute, Router } from '@angular/router';
import { GuardiansService } from '../../../../service/guardians.service';
import { Guardians } from '../../../../guardians';
import { KidsService } from '../../../../service/kids.service';
import { Kids } from '../../../../kids';
import { EventService } from '../../../../service/event.service';
import { events } from '../../../../events';
import { MeetingpointsService } from '../../../../service/meetingpoints.service';
import { FormControl } from '@angular/forms';
import { ConstantsService } from '../../../../service/constants.service';
import { Location } from '@angular/common';

@Component({
  selector: 'kt-booking-create',
  templateUrl: './booking-create.component.html',
  styleUrls: ['./booking-create.component.scss']
})
export class BookingCreateComponent implements OnInit {

  myControl: FormControl = new FormControl();

  statuses = [];
  methods = [];
  options = [];
  teams = [];
  tshirts: any = [];
  items: Booking[] = [];
  Guardians: Guardians[] = [];
  kid: Kids[] = [];
  events: events[] = [];
  id = null;
  approval = null;
  subteam = null;
  team = null;
  cabin = null;
  tshirt = null;
  event_id = null;
  room_no = null;
  paymentStatus = null;
  paymentAmount = null;
  paymentMethod = null;
  meetingpoint_id = null;
  paymentAvailability = null;
  points: Array<any> = [];

  constructor(public snackBar: MatSnackBar, private constant: ConstantsService,
    private service: BookingService,
    public location: Location,
    private KidsService: KidsService,
    private GuardiansService: GuardiansService,
    private EventService: EventService,
    public point: MeetingpointsService,
    public router: Router,
    private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.tshirts = this.constant.tshirts;
    this.teams = this.constant.teams;
    this.options = this.constant.options;
    this.methods = this.constant.paymentMethod;
    this.statuses = this.constant.paymentStatus;
  }

  ngOnInit() {

    // this.GuardiansService.list().subscribe((data)=>{this.Guardians = data;});

    this.KidsService.get(this.id).subscribe((data2) => {
      this.kid = data2;
  //    // console.log(this.kid);
    });

    this.point.list().subscribe((data) => {
      this.points = data;
      // console.log(this.points);
    });

    this.EventService.list().subscribe((data3) => {
      this.events = data3;
      // console.log(this.events);
    });

  }

  onSubmit(form) {

    const obj = {
      kid_id: this.id,
      event_id: form.value.event_id,
      paymentAmount: form.value.paymentAmount,
      tshirt: form.value.tshirt,
      cabin: form.value.cabin,
      team: form.value.team,
      subteam: form.value.subteam,
      attendedBefore: form.value.attendedBefore,
      attendedTeam: form.value.attendedTeam,
      approval: form.value.approval,
      paymentAvailability: form.value.paymentAvailability,
      paymentStatus: form.value.paymentStatus,
      room_no: form.value.room_no,
      meetingpoint_id: form.value.meetingpoint_id,
      paymentMethod: form.value.paymentMethod
    };
    // console.log(obj);
    this.service.create(this.id, obj).subscribe((data) => { });
    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/booking'], { relativeTo: this.activatedRoute });
  }
}
