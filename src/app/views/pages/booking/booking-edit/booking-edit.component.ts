import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { BookingService } from '../../../../service/booking.service';
import { Booking } from '../../../../booking';
import { ActivatedRoute, Router } from '@angular/router';

import { GuardiansService } from '../../../../service/guardians.service';
import { Guardians } from '../../../../guardians';

import { KidsService } from '../../../../service/kids.service';
import { Kids } from '../../../../kids';

import { EventService } from '../../../../service/event.service';
import { events } from '../../../../events';

import { MeetingpointsService } from '../../../../service/meetingpoints.service';
import { Location } from '@angular/common';
import { ConstantsService } from '../../../../service/constants.service';

@Component({
  selector: 'kt-booking-edit',
  templateUrl: './booking-edit.component.html',
  styleUrls: ['./booking-edit.component.scss']
})
export class BookingEditComponent implements OnInit {


  statuses = [];
  methods = [];
  options = [];
  teams = [];
  tshirts: any = [];
  sports: any = [];
  items: Booking[] = [];
  Guardians: Guardians[] = [];
  KidsData: Kids[] = [];
  events: events[] = [];
  id = null;

  points: Array<any> = [];

  constructor(public snackBar: MatSnackBar, private location: Location, public router: Router, private activatedRoute: ActivatedRoute,
    private service: BookingService, private KidsService: KidsService, private constant: ConstantsService,
    private GuardiansService: GuardiansService, private EventService: EventService, public point: MeetingpointsService) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.service.get(this.id).subscribe((data) => {
      this.items = data;
      // console.log(this.items)
      // console.log(this.id);
    });
    this.teams = this.constant.teams;
    this.options = this.constant.options;
    this.methods = this.constant.paymentMethod;
    this.statuses = this.constant.paymentStatus;
  }

  ngOnInit() {

    this.service.getTshirts().subscribe((data) => {
      this.tshirts = data;

    });
    this.service.getSports().subscribe((data) => {
      this.sports = data;

    });


    this.GuardiansService.list().subscribe((data) => {
      this.Guardians = data;
      // console.log(this.Guardians)
    });

    this.KidsService.list().subscribe((data2) => {
      this.KidsData = data2;
    });

    this.point.list().subscribe((data) => {
      this.points = data;
    });

    this.EventService.list().subscribe((data3) => { this.events = data3; });
  }

  onSubmit(form) {
    const obj = {
      event_id: form.value.event_id,
      paymentAmount: form.value.paymentAmount,
      tshirt: form.value.tshirt,
      cabin: form.value.cabin,
      team: form.value.team,
      subteam: form.value.subteam,
      attendedBefore: form.value.attendedBefore,
      attendedTeam: form.value.attendedTeam,
      approval: form.value.approval,
      paymentAvailability: form.value.paymentAvailability,
      paymentStatus: form.value.paymentStatus,
      room_no: form.value.room_no,
      meetingpoint_id: form.value.meetingpoint_id,
      paymentMethod: form.value.paymentMethod,
      sport1: form.value.first_sport,
      sport2: form.value.second_sport,
      sport3: form.value.third_sport
    };
    // console.log(obj);
    this.service.edit(this.id, obj).subscribe((data) => {
      setTimeout(() => {
        this.cancel();
      }, 500);
    });

  }

  cancel() {
    this.location.back();
    return;
    this.location.back();
  }
}
