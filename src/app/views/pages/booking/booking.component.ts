import { Component, OnInit, ChangeDetectionStrategy, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BookingService } from '../../../service/booking.service';
import { Booking } from '../../../booking';
import { EventService } from '../../../service/event.service';
import { HomeService } from '../../../service/home.service';
import { events } from '../../../events';
import { ActivatedRoute, Router } from '@angular/router';

import { SelectionModel } from '@angular/cdk/collections';

@Component({
	selector: 'kt-booking',
	templateUrl: './booking.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, AfterViewInit, OnDestroy {
	items: Booking[] = [];
	events: events[] = [];
	selection = new SelectionModel(true, []);
	camptype: string;
	resultsLength: number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();
	displayedColumns1: string[] = ['select', 'firstName', 'paymentStatus', 'paymentMethod', 'created_at', 'actions'];

	approval = "";
	options = ['Show All', 'Unpaid', 'Paid', 'Waiting Paid', 'Waiting Unpaid', 'Canceled', 'Refunded'];
	// status = ['Finished','Waiting List'];
	// payment = ['Done','Refunded','Not Yet'];
	method = ['Show All', 'Cash', 'Online'];
	filter = {
		yearValue: -1,
		campValue: -1,
		method_id: '',
		status_id: '',
		searchKey: '',
		showGards: false
	}

	years: any[] = [];
	camps: any[] = [];

	constructor(private service: BookingService, public home: HomeService, public evnt: EventService, public router: Router, private activatedRoute: ActivatedRoute) {
	}
	ngOnDestroy(): void {
		localStorage.setItem("BookingFilter", JSON.stringify(this.filter))
	}

	Reset() {
		// console.log("Reset")
		this.dataSource.filter = '';
		this.filter.yearValue = -1;
		this.filter.campValue = -1;
		this.filter.method_id = '';
		this.filter.status_id = '';
		this.filter.searchKey = '';
		this.filter.showGards = false;
		this.camptype = null;
		this.approval = ""
		localStorage.setItem("BookingFilter", JSON.stringify(this.filter))
		this.getCampData();
	}

	ngOnInit() {
		this.home.getYears().subscribe((data) => {
			this.years = data;
			// console.log(data);
			let tempFilter = JSON.parse(localStorage.getItem("BookingFilter"));
			if (tempFilter) {
				this.filter = tempFilter;
			} else {
				this.filter.yearValue = this.years[0].year
			}
			this.home.getCamps(this.filter.yearValue).subscribe((data) => {
				this.camps = data;
				// console.log(data)
				this.filter.campValue = data.length > 0 ? data[0].id : -1;
				this.camptype = data.length > 0 ? data[0]["type"] : ""
				// console.log(this.camps)
				this.getCampData();
			});
		});
		this.evnt.list().subscribe((data) => {
			this.events = data;
			// console.log(this.events)
		});


	}
	ngAfterViewInit() {
		// console.log()
	}
	temp = [];
	ShowGards(evt) {
		// console.log(evt, this.filter.showGards)
		if (evt.checked) {
			this.items = this.items.filter(item => item["primary_user"] != null);
			this.dataSource.data = this.items;
			this.dataSource.data.sort(this.sortDate)
			this.resultsLength = this.items.length;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			// console.log(this.items);
		} else {
			this.items = this.temp;
			this.dataSource.data = this.items;
			this.dataSource.data.sort(this.sortDate)
			this.resultsLength = this.items.length;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			// console.log(this.items);
			this.approval = "";
		}
	}
	Approve() {
		// // console.log(evt)
		if (this.selection.selected.length > 0) {
			let family_id = this.selection.selected[0].familyID;
			let event_id = this.selection.selected[0].event_id;
			let body = {
				event_id: event_id,
				familyID: family_id,
				approval: this.approval
			}
			// console.log(body)
			// console.log(this.service.servic.baseAppUrl + "editFamily")
			this.service.httpClient.get<Booking[]>("https://kgcdash.kidsgamescamp.com/api/editFamily/" + event_id + "/" + family_id + "/" + this.approval).subscribe(res => {
				// console.log(res)
				if (res["status"])
					alert("Applied successfully");
				else {
					alert("Something went wrong,Please try again");
					this.approval = "";

				}
			}, err => {
				alert("Something went wrong,Please try again");
				this.approval = "";
			})
		} else {
			alert("Please select atleast one guardian");
		}
		//booking.php?do=editFamily
	}
	statsEvents(id) {
		this.service.getByEvent(id, 0).subscribe((data) => {
			// console.log(data)
			this.items = data;
			this.temp = data;
			this.items.sort(this.sortDate)
			this.dataSource.data = data;
			this.dataSource.data.sort(this.sortDate)
			this.resultsLength = data.length;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}
	SelectAll(event) {
		this.approval = "";
		// console.log(event)
		event ? this.masterToggle() : null
	}

	selectChangeHandler() {
		this.home.getCamps(this.filter.yearValue).subscribe((data) => {
			this.camps = data;
			this.filter.campValue = this.camps[0].id;
			// console.log(this.camps)
		});
	}

	getCampData() {
		let temp = this.camps.filter(item => item.id == this.filter.campValue);
		if (temp.length > 0)
			this.camptype = temp[0].type;
		this.filter.showGards = false;
		this.statsEvents(this.filter.campValue);
	}
	SelectRow(event, selection, row) {
		// console.log(event, selection)
		this.approval = ""
		event ? selection.toggle(row) : null;
	}
	applyFilter(filterValue: string) {
		if (filterValue == "Show All") {
			this.dataSource.filter = '';
			return;
		}
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
		this.dataSource.filter = filterValue;
		// this.dataSource
		// this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	getItemCssClass(team: string): string {
		switch (team) {
			case 'Waiting List':
				return 'danger';
			case 'Reservation Done':
				return 'success';
			case 'Refunded':
				return 'danger';
			case 'Done':
				return 'success';
		}
		return '';
	}

	edit(id) {
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
	}

	add() {
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	kid(id) {
		this.router.navigate(['../kids/view', id], { relativeTo: this.activatedRoute });
	}

	delete(id) {
		var r = confirm("Do you want to delete this item?");
		if (r == true) {
			this.service.delete(id).subscribe((data) => {
				this.evnt.list().subscribe((data) => {
					this.events = data;
					this.getCampData();
				});
			});
		} else { return false; }

		setTimeout(() => {
			this.ngOnInit();
		}, 1000);
	}

	sortDate(a) {
		var dateA = new Date(a.paymentDate);
		var dateB = new Date(a.paymentDate);
		return dateA > dateB ? 1 : -1;
	};






	//selection
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.items.length;
		return numSelected === numRows;
	}

	/**
	 * Selects all rows if they are not all selected; otherwise clear selection
	 */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.items.forEach(row => this.selection.select(row));
		}
	}

	send(type) {
		// console.log(this.selection.selected)
		if (type == 1) { /// Send Email
			if (this.selection.selected.length < 1) {
				alert("Please select at least one recoerd");
				return;
			} else {
				let event_id = this.selection.selected[0].event_id;
				let ids = []
				for (let i = 0; i < this.selection.selected.length; i++) {
					ids.push(this.selection.selected[i].id)
				}
				let body = {
					ids: ids,
					event_id: event_id,
					paid_type: 0
				}
				// console.log(body)
				// console.log("https://kgcdash.kidsgamescamp.com/api/SendEmails")
				this.service.httpClient.post<Booking[]>("https://kidsgamescamp.com/api/SendEmails", body).subscribe(res => {
					// console.log(res)
					if (res["status"])
						alert("Applied successfully");
					else {
						alert("Something went wrong,Please try again");

					}
				}, err => {
					alert("Something went wrong,Please try again");
				})
			}
		} else if (type == 2) { /// Send SMS

		}
	}



}
