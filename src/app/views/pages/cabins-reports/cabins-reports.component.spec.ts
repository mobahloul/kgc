import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinsReportsComponent } from './cabins-reports.component';

describe('CabinsReportsComponent', () => {
  let component: CabinsReportsComponent;
  let fixture: ComponentFixture<CabinsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
