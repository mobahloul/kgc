import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { TeamsService } from '../../../service/teams.service';
import { Teams } from  '../../../teams';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-cabins-reports',
  templateUrl: './cabins-reports.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./cabins-reports.component.scss']
})
export class CabinsReportsComponent implements OnInit {
	teams: Teams[]=[];
	cabins: any[] =[];

	mainteams = [{'name':'Male','color':'primary'},
	{'name':'Female','color':'danger'}];
rooms = [1,2,3,4,5,6,7,8,9,10];

	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['name','Total','#1', '#2', '#3', '#4','#5','#6','#7','#8','#9','#10','actions'];

	constructor(public snackBar: MatSnackBar, private service: TeamsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.cabinsReport().subscribe((data)=>{
			this.teams = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
    
	  details(id){
      this.router.navigate(['details', id], { relativeTo: this.activatedRoute });
    }

}
