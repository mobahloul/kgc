import { Component, OnInit } from '@angular/core';
// import { MatSnackBar } from '@angular/material';
import { TeamsService } from '../../../../service/teams.service';
import { Teams } from  '../../../../teams';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-cabin-details',
  templateUrl: './cabin-details.component.html',
  styleUrls: ['./cabin-details.component.scss']
})
export class CabinDetailsComponent implements OnInit {

  id=null;
  items: Teams[]=[];
  constructor( private service: TeamsService, public router: Router,private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(){
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.service.getcabins(this.id).subscribe((data)=>{
      this.items = data;
    });
  }

  get_color(item){
    switch (item) {
      case 'Male':
        return 'primary';
        break;

        case 'Female':
          return 'danger';
        break;
    }
  }

}
