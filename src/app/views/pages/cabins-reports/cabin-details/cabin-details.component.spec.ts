import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinDetailsComponent } from './cabin-details.component';

describe('CabinDetailsComponent', () => {
  let component: CabinDetailsComponent;
  let fixture: ComponentFixture<CabinDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
