import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MeetingpointsService } from '../../../../service/meetingpoints.service';
import { Points } from '../../../../meetingpoints';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-point-edit',
  templateUrl: './point-edit.component.html',
  styleUrls: ['./point-edit.component.scss']
})
export class PointEditComponent implements OnInit {

  Points: Points[] = [];
  id = null;

  constructor(public location: Location, public snackBar: MatSnackBar, private pont: MeetingpointsService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.pont.get(this.id).subscribe((data) => {
      this.Points = data;
    });
  }

  onSubmit(form) {

    const obj = {
      point: form.value.point,
      address: form.value.address,
      area: form.value.area,
      location: form.value.location,
      updated_by: form.value.updated_by
    };

    this.pont.edit(this.id, obj).subscribe((data) => {
    });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/meeting-points'], { relativeTo: this.activatedRoute });
  }
}
