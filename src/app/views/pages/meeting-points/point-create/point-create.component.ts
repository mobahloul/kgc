import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MeetingpointsService } from '../../../../service/meetingpoints.service';
import { Points } from '../../../../meetingpoints';
import { ActivatedRoute, Router } from '@angular/router';

export const MY_FORMATS = {
	parse: {
		dateInput: 'LL',
	},
	display: {
		dateInput: 'YYYY-MM-DD',
		monthYearLabel: 'YYYY',
		dateA11yLabel: 'LL',
		monthYearA11yLabel: 'YYYY',
	},
};

@Component({
	selector: 'kt-point-create',
	templateUrl: './point-create.component.html',
	styleUrls: ['./point-create.component.scss']
})
export class PointCreateComponent implements OnInit {

	address = null;
	area = null;
	location = null;
	created_by = null;
	updated_by = null;
	point = null;
	Points: Points[] = [];

	constructor(public snackBar: MatSnackBar, private pont: MeetingpointsService, public router: Router, private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() {
	}

	onSubmit(form) {

		const obj = {
			point: form.value.point,
			address: form.value.address,
			area: form.value.area,
			location: form.value.location,
			created_by: form.value.created_by,
			updated_by: form.value.updated_by
		};
		this.pont.create(obj).subscribe((data) => {
		});

		setTimeout(() => {
			this.cancel();
		}, 1000);
	}

	cancel() {
		this.location.back();
		return;
		this.router.navigate(['/default/meeting-points'], { relativeTo: this.activatedRoute });
	}

}
