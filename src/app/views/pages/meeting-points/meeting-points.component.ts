import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { MeetingpointsService } from '../../../service/meetingpoints.service';
import { Points } from  '../../../meetingpoints';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-meeting-points',
  templateUrl: './meeting-points.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./meeting-points.component.scss']
})
export class MeetingPointsComponent implements OnInit {

	Points: Points[]=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
	displayedColumns1: string[] = ['point','address','area','actions'];

	constructor(public snackBar: MatSnackBar,  public point: MeetingpointsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.point.list().subscribe((data)=>{
			this.Points = data;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
		
	}

	delete(id){
		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.point.delete(id).subscribe((data)=>{});
		}else{ return false;}

		setTimeout(() => {
            this.ngOnInit();
          }, 1000);
		
	}
}