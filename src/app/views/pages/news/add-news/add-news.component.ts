import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NewsService } from '../../../../service/news.service';
import { News } from '../../../../news';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'kt-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.scss']
})
export class AddNewsComponent implements OnInit {
  postTitle: string;
  showimage: boolean = false;
  postImage;
  postContent: any;
  postDesc: any;
  url: string;
  constructor(public location: Location, public snackBar: MatSnackBar, public service: NewsService, public router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
  }
  onSubmit(f) {
    let formdata = new FormData();
    formdata.append('title', this.postTitle);
    formdata.append('description', this.postDesc);
    formdata.append('image', this.postImage);
    formdata.append('content', this.postContent);
    this.service.create(formdata).subscribe((data) => {
      if (data) {
        setTimeout(() => {
          this.cancel();
        }, 1000);
      }
    }, (error) => {
      // console.log(error);
    });
    // console.log(formdata);
  }
  onSelectFile(fileList): void {
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let self = this;
    let imageurl;
    self.postImage = file;
    fileReader.onload = (e: any) => {

      (<HTMLImageElement>document.getElementById('image')).src = e.target.result as string;

    }
    fileReader.readAsDataURL(file);
  }
  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/news'], { relativeTo: this.activatedRoute });
  }

}


