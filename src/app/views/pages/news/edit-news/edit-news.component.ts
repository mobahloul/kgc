
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NewsService } from '../../../../service/news.service';

import { News } from '../../../../news';
import { ActivatedRoute, Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class EditNewsComponent implements OnInit {
  item: News;
  id = null;


  postImage: string;
  postim: string = '';
  postImage_file;
  postTitle: string;
  postDesc: string;
  postContent;
  genderOptions = [
    { key: 'Male', value: "Male" },
    { key: 'Female', value: "Female" },
    { key: 'Mix', value: "Mix" }
  ];

  constructor(public location: Location, public snackBar: MatSnackBar, public service: NewsService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.service.get(this.id).subscribe((data) => {
      this.postTitle = data.title;
      this.postDesc = data.description;
      this.postContent = data.topic;
      this.postImage = data.image;
    });
  }
  onSubmit(f) {
    let formdata = new FormData();
    formdata.append('title', this.postTitle);
    formdata.append('description', this.postDesc);
    if (this.postImage_file) {
      formdata.append('image', this.postImage_file);
    }

    formdata.append('content', this.postContent);
    this.service.edit(this.id, formdata).subscribe((data) => {
      if (data) {
        setTimeout(() => {
          this.cancel();
        }, 1000);
      }
    }, (error) => {
      // console.log(error);
    });
    // console.log(formdata);
  }
  onSelectFile(fileList): void {
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let self = this;
    let imageurl;
    self.postImage_file = file;
    fileReader.onload = (e: any) => {

      (<HTMLImageElement>document.getElementById('image')).src = e.target.result as string;

    }
    fileReader.readAsDataURL(file);
  }


  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/news'], { relativeTo: this.activatedRoute });
  }

}
