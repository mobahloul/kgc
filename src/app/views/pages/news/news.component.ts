import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { NewsService } from '../../../service/news.service';
import { News } from  '../../../news';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'kt-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

	items: any=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
  displayedColumns1: string[] = ['productName','status','date','actions'];
  
	constructor(public snackBar: MatSnackBar,  public service: NewsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.list().subscribe((data)=>{
			this.items = data;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
	}

   delete(id){
		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.service.delete(id).subscribe((data)=>{});
        }else{ return false;}
		setTimeout(() => {
			this.ngOnInit();
        }, 1000);
	}

}