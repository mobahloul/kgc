import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { KidsService } from '../../../../service/kids.service';
import { SportsService } from '../../../../service/sports.service';
import { Kids } from '../../../../kids';
import { ActivatedRoute, Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';
import { ConstantsService } from '../../../../service/constants.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-kid-edit',
  templateUrl: './kid-edit.component.html',
  styleUrls: ['./kid-edit.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class KidEditComponent implements OnInit {

  items: Kids[] = [];
  id = null;
  sportsitems: any;
  uploadsLink: any;
  options = [];
  relations = [];
  genders = [];
  teams = [];
  profileImage: File;
  birthCertificate: File;
  tshirts: any = [];
  types = [
    { key: 'National ID', value: "Egypt" },
    { key: 'Passport', value: "Other" }
  ];

  constructor(public snackBar: MatSnackBar, private location: Location, private constant: ConstantsService,
    private sports: SportsService, public service: KidsService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.uploadsLink = this.constant.uploadsLink;
    this.tshirts = this.constant.tshirts;
    this.teams = this.constant.teams;
    this.genders = this.constant.genders;
    this.options = this.constant.options;
    this.relations = this.constant.relations;
  }

  ngOnInit() {
    this.service.get(this.id).subscribe((data) => {
      this.items = data;
    });

    this.sports.list().subscribe((data) => {
      this.sportsitems = data;
    });

  }

  onFileChanged(event) {
    const file = event.target.files[0]
  }

  onProfileImage(event) {
    this.profileImage = event.target.files[0];
  }

  onBirthCertificate(event) {
    this.birthCertificate = event.target.files[0];
  }

  onSubmit(form) {

    const obj = {
      firstName: form.value.firstName,
      middleName: form.value.middleName,
      familyName: form.value.familyName,
      team: form.value.team,
      gender: form.value.gender,
      birthDate: form.value.birthDate,
      email: form.value.email,
      mobile: form.value.mobile,
      address: form.value.address,
      school: form.value.school,
      grade: form.value.grade,
      IDType: form.value.IDType,
      nationalID: form.value.nationalID,
      parent1_relation: form.value.parent1_relation,
      parent2_relation: form.value.parent2_relation,
      emergeContactNumber: form.value.emergeContactNumber,
      sport1_id: form.value.sport1_id,
      sport2_id: form.value.sport2_id,
      sport3_id: form.value.sport3_id,
      sport4_id: form.value.sport4_id,
      sport5_id: form.value.sport5_id,
      medicalIssues: form.value.medicalIssues,
      foodAllergy: form.value.foodAllergy,
      ongoingMedication: form.value.ongoingMedication,
      medicalIssues_details: form.value.medicalIssues_details,
      foodAllergy_details: form.value.foodAllergy_details,
      ongoingMedication_details: form.value.ongoingMedication_details,
      // profileImage:  this.profileImage,
      // birthCertificate:  this.birthCertificate,
    };
    // console.log(obj);
    this.service.edit(this.id, obj).subscribe((data) => {
      this.cancel();
    });

    // setTimeout(() => {
    //   this.cancel();
    //       }, 1000);
  }

  cancel() {
    this.location.back();
  }
}
