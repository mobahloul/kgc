import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { KidsService } from '../../../../service/kids.service';
import { SportsService } from '../../../../service/sports.service';
import { Kids } from '../../../../kids';
import { ActivatedRoute, Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ConstantsService } from '../../../../service/constants.service';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-kid-create',
  templateUrl: './kid-create.component.html',
  styleUrls: ['./kid-create.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class KidCreateComponent implements OnInit {
  firstName = null;
  middleName = null;
  familyName = null;
  team = null;
  gender = null;
  birthDate = null;
  email = null;
  mobile = null;
  address = null;
  school = null;
  grade = null;

  nationalID = null;
  parent1_relation = null;
  parent2_relation = null;
  emergeContactNumber = null;
  sport1_id = null;
  sport2_id = null;
  sport3_id = null;
  sport4_id = null;
  sport5_id = null;



  medicalIssues_details = null;
  foodAllergy_details = null;
  ongoingMedication_details = null;
  items: Kids[] = [];
  familyID = null;
  sportsitems: any;

  types = [
    { key: 'National ID', value: "Egypt" },
    { key: 'Passport', value: "Other" }
  ];
  medicalIssues = 'No';
  foodAllergy = 'No';
  ongoingMedication = 'No';
  IDType = 'National ID';
  options = [];
  relations = [];
  genders = [];
  teams = [];
  tshirts: any = [];

  constructor(public location: Location, public snackBar: MatSnackBar, public service: KidsService, private sports: SportsService, private constant: ConstantsService,
    public router: Router, private activatedRoute: ActivatedRoute) {
    this.familyID = this.activatedRoute.snapshot.paramMap.get('id');
    this.tshirts = this.constant.tshirts;
    this.teams = this.constant.teams;
    this.genders = this.constant.genders;
    this.options = this.constant.options;
    this.relations = this.constant.relations;

  }

  ngOnInit() {
    this.sports.list().subscribe((data) => {
      this.sportsitems = data;
    });
  }

  onSubmit(form) {

    const obj = {
      firstName: form.value.firstName,
      middleName: form.value.middleName,
      familyName: form.value.familyName,
      team: form.value.team,
      gender: form.value.gender,
      birthDate: form.value.birthDate,
      email: form.value.email,
      mobile: form.value.mobile,
      address: form.value.address,
      school: form.value.school,
      grade: form.value.grade,
      IDType: form.value.IDType,
      nationalID: form.value.nationalID,
      parent1_relation: form.value.parent1_relation,
      parent2_relation: form.value.parent2_relation,
      emergeContactNumber: form.value.emergeContactNumber,
      sport1_id: form.value.sport1_id,
      sport2_id: form.value.sport2_id,
      sport3_id: form.value.sport3_id,
      sport4_id: form.value.sport4_id,
      sport5_id: form.value.sport5_id,
      medicalIssues: form.value.medicalIssues,
      foodAllergy: form.value.foodAllergy,
      ongoingMedication: form.value.ongoingMedication,
      medicalIssues_details: form.value.medicalIssues_details,
      foodAllergy_details: form.value.foodAllergy_details,
      ongoingMedication_details: form.value.ongoingMedication_details
    };
    this.service.create(this.familyID, obj).subscribe((data) => {
    });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/kids'], { relativeTo: this.activatedRoute });
  }
}
