import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { KidsService } from '../../../service/kids.service';
import { Kids } from  '../../../kids';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-kids',
  templateUrl: './kids.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./kids.component.scss']
})

export class KidsComponent implements OnInit {
	Kids: Kids[]=[];

	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 

	genderOptions = [
		{ key: 'Male', value: "Male" },
		{ key: 'Female', value: "Female" }
	  ];

	displayedColumns1: string[] = ['firstName', 'gender', 'grade', 'mobile','nationalID','team','guardian','booking','actions'];

	constructor(public snackBar: MatSnackBar, private service: KidsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.list().subscribe((data)=>{
			this.Kids = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }

	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });		
	}

	view(id){
		this.router.navigate(['view', id], { relativeTo: this.activatedRoute });		
	}

	view_guard(id: number){
		this.router.navigate(['/default/guardians/view', id], { relativeTo: this.activatedRoute });
	}

	delete(id){

		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.service.delete(id).subscribe((data)=>{});
        }else{ return false;}
		  	
		setTimeout(() => {
			this.ngOnInit();
				}, 1000);
	}


	add_booking(id){
		this.router.navigate(['/default/booking/create', id], { relativeTo: this.activatedRoute });
	}

}
