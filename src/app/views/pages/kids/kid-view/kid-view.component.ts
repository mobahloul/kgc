import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { KidsService } from '../../../../service/kids.service';
import { Kids } from  '../../../../kids';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { ConstantsService } from '../../../../service/constants.service';
@Component({
  selector: 'kt-kid-view',
  templateUrl: './kid-view.component.html',
  styleUrls: ['./kid-view.component.scss']
})
export class KidViewComponent implements OnInit {
  items: Kids[]=[];
  id=null;
  uploadsLink:string;
  constructor(public snackBar: MatSnackBar,private location: Location, public service: KidsService, 
    public router: Router, private constant:ConstantsService,private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');  
    this.uploadsLink = this.constant.uploadsLink;
  }

  ngOnInit() {
    this.service.get(this.id).subscribe((data)=>{
			this.items = data;
    });
  }

  cancel(){
    this.location.back();
  }

  edit(id){
    this.router.navigate(['/default/kids/edit', id], { relativeTo: this.activatedRoute });
 }

}
