import { Component, OnInit, ChangeDetectionStrategy, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { HomeService } from '../../../service/home.service';
import { BookingService } from '../../../service/booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../../../service/event.service';
import { events } from '../../../events';
import { Booking } from '../../../booking';
import { MeetingpointsService } from '../../../service/meetingpoints.service';
import { ConstantsService } from '../../../service/constants.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
	selector: 'kt-teams',
	templateUrl: './teams.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit, OnDestroy {
	events: events[] = [];
	resultsLength: number;
	selection = new SelectionModel(true, []);
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();
	displayedColumns1: string[] = ['select', 'firstName', 'team', 'subteam', 'gender', 'cabin', 'meetingpoint', 'tshirt', 'actions'];
	filter = {
		yearValue: -1,
		campValue: -1,
		search_key: '',
		point_id: -1,
		gender_id: -1,
		tshirt_id: -1,
		team_id: -1
	}


	years: any[] = [];
	camps: any[] = [];
	items: Booking[] = [];
	meetingpoints = [];

	teams = [];

	tshirts = [];

	genders = [];

	constructor(public snackBar: MatSnackBar, public home: HomeService,
		private service: BookingService, private constant: ConstantsService,
		private mp: MeetingpointsService,
		public evnt: EventService, public router: Router,
		private activatedRoute: ActivatedRoute) {
		this.tshirts = this.constant.tshirts;
		this.teams = this.constant.teams;
		this.genders = this.constant.genders;
	}

	ngOnDestroy(): void {
		localStorage.setItem("TeamFilter", JSON.stringify(this.filter))
	}

	Reset() {
		// console.log("Reset")
		this.dataSource.filter = '';
		this.filter = {
			yearValue: -1,
			campValue: -1,
			search_key: '',
			point_id: -1,
			gender_id: -1,
			tshirt_id: -1,
			team_id: -1
		}
		localStorage.setItem("TeamFilter", JSON.stringify(this.filter))
		this.getCampData();
	}

	ngOnInit() {
		this.evnt.list().subscribe((data) => {
			this.events = data;
		});

		this.mp.list().subscribe(data => {
			this.meetingpoints.push({ point: "Show All" })
			if (data && data.length > 0)
				for (let i = 0; i < data.length; i++)
					this.meetingpoints.push(data[i]);
			// console.log(data);
		});

		this.home.getYears().subscribe((data) => {
			this.years = data;

			let tempFilter = JSON.parse(localStorage.getItem("TeamFilter"));
			if (tempFilter) {
				this.filter = tempFilter;
			} else {
				this.filter.yearValue = this.years[0].year
			}

			this.home.getCamps(this.filter.yearValue).subscribe((data) => {
				this.camps = data;
				this.filter.campValue = this.camps[0].id;
				this.getCampData();
			});

		});

		this.service.list().subscribe((data) => {
			this.items = data;
			this.dataSource.data = data;
			this.resultsLength = data.length;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});

	}
	sortYears(a) {
		var dateA = a[0];
		var dateB = a[0];
		return dateA > dateB ? 1 : -1;
	};

	selectChangeHandler() {
		this.home.getCamps(this.filter.yearValue).subscribe((data) => {
			this.camps = data;
		});

	}

	getCampData() {
		this.service.getByEvent(this.filter.campValue, 1).subscribe((data) => {
			this.items = data;
			this.dataSource.data = data;
			this.resultsLength = data.length;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});

	}

	applyFilter(filterValue: string) {
		// console.log(filterValue)
		this.dataSource.data = this.items;
		if (filterValue == "Show All") {
			this.dataSource.filter = '';
			return;
		}

		if (filterValue == "Male") {
			// filterValue = filterValue.trim(); // Remove whitespace
			// filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
			// this.dataSource.filter = filterValue;
			this.dataSource.data = this.dataSource.data.filter(item => item["gender"] == filterValue);

			// console.log(this.dataSource.filteredData)
			return
		}
		this.dataSource.data = this.items;
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
		this.dataSource.filter = filterValue;

		// console.log(this.dataSource.filteredData)
	}


	getItemCssClassByTeam(team: string): string {
		switch (team) {
			case 'Alpha':
				return 'primary';
			case 'Omega':
				return 'success';
		}
		return '';
	}

	edit(id) {
		this.router.navigate(['/default/booking/edit', id], { relativeTo: this.activatedRoute });
	}

	add() {
		this.router.navigate(['/default/booking/create'], { relativeTo: this.activatedRoute });
	}

	kid(id) {
		this.router.navigate(['/default/kids/view', id], { relativeTo: this.activatedRoute });
	}



	//selection
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.items.length;
		return numSelected === numRows;
	}

	/**
	 * Selects all rows if they are not all selected; otherwise clear selection
	 */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.items.forEach(row => this.selection.select(row));
		}
	}

	send(type) {
		// console.log(this.selection.selected)
		if (type == 1) { /// Send Email
			if (this.selection.selected.length < 1) {
				alert("Please select at least one recoerd");
				return;
			} else {
				let event_id = this.selection.selected[0].event_id;
				let ids = []
				for (let i = 0; i < this.selection.selected.length; i++) {
					ids.push(this.selection.selected[i].id)
				}
				let body = {
					ids: ids,
					event_id: event_id,
					paid_type: 1
				}
				// console.log(body)
				// console.log("https://kgcdash.kidsgamescamp.com/api/SendEmails")
				this.service.httpClient.post<Booking[]>("https://kidsgamescamp.com/api/SendEmails", body).subscribe(res => {
					// console.log(res)
					if (res["status"])
						alert("Applied successfully");
					else {
						alert("Something went wrong,Please try again");

					}
				}, err => {
					alert("Something went wrong,Please try again");
				})
			}
		} else if (type == 2) { /// Send SMS

		}
	}

}
