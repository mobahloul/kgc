import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TeamsService } from '../../../../service/teams.service';
import { MeetingpointsService } from '../../../../service/meetingpoints.service';
import { Teams } from '../../../../teams';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'kt-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  items: Teams[] = [];
  id = null;

  team = [
    { key: 'Alpha', value: "Alpha" },
    { key: 'Omega', value: "Omega" }
  ];

  tshirt = [
    { key: '8', value: "8" },
    { key: '10', value: "10" },
    { key: '12', value: "12" },
    { key: '14', value: "14" },
    { key: '16', value: "16" },
    { key: 'S', value: "S" },
    { key: 'M', value: "M" },
    { key: 'L', value: "L" },
    { key: 'XS', value: "XS" },
    { key: 'XL', value: "XL" },
    { key: 'XXL', value: "XXL" }
  ];


  points: Array<any> = [];

  constructor(public location: Location, public snackBar: MatSnackBar, public service: TeamsService, public point: MeetingpointsService, public router: Router, private activatedRoute: ActivatedRoute) {
    // this.stateCtrl = new FormControl();
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.service.get(this.id).subscribe((data) => {
      this.items = data;
    });

    this.point.list().subscribe((data) => {
      this.points = data;
    });

  }

  onSubmit(form) {

    const obj = {
      team: form.value.team,
      subteam: form.value.subteam,
      cabin: form.value.cabin,
      tshirt: form.value.tshirt,
      meetingPoint_id: form.value.meetingPoint_id,
    };

    this.id = form.value.id;
    this.service.edit(this.id, obj).subscribe((data) => {
      // console.log(data);
    });
    // window.location.href='/default/kids';
    this.cancel();

  }


  cancel() {
    this.location.back();
    return;
    this.router.navigate(['teams'], { relativeTo: this.activatedRoute });
  }
}
