import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { EventService } from '../../../service/event.service';
import { HomeService } from '../../../service/home.service';
import { BookingService } from '../../../service/booking.service';
import { ReportsService } from '../../../service/reports.service';
import { events } from '../../../events';
import { Booking } from '../../../booking';
import { ActivatedRoute, Router } from '@angular/router';
// Services
import { LayoutConfigService } from '../../../core/_base/layout';
import { SparklineChartOptions } from '../../../core/_base/metronic';
import { MeetingpointsService } from '../../../service/meetingpoints.service';
import { SportsService } from '../../../service/sports.service';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';

export interface TimelineData {
	time: string;
	text: string;
	icon?: string;
}

@Component({
	selector: 'kt-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

	@Input() timeline: TimelineData[];

	options = ['Unpaid', 'Paid', 'Waiting Paid', 'Waiting Unpaid', 'Canceled', 'Refunded'];
	method = ['Cash', 'Online'];
	teamsOptions = [
		{ key: 'Alpha', value: "Alpha" },
		{ key: 'Omega', value: "Omega" }
	];
	selected2 = this.teamsOptions[0];
	tshirts = ['xs', 's', 'm', 'l', 'xl'];
	genders = ['Male', 'Female'];
	values = [
		{ key: 'Yes', value: "Yes" },
		{ key: 'No', value: "No" }
	];


	yearValue: number;
	campValue: number;
	campID: number = 1;
	campType: string = "";

	stateCtrl: FormControl;
	events: events[] = [];
	items: Booking[] = [];
	meetingpoints: any;
	sports: any;
	lastbooking: any[] = [];
	years: any[] = [];
	camps: any[] = [];

	chartOptions1: SparklineChartOptions;
	chartOptions2: SparklineChartOptions;
	chartOptions3: SparklineChartOptions;
	chartOptions4: SparklineChartOptions;

	resultsLength1: number;
	@ViewChild('sort1') sort1: MatSort;
	@ViewChild('paginator') paginator: MatPaginator;
	public dataSource1 = new MatTableDataSource();
	displayedColumns1: string[] = ['firstName', 'paymentStatus', 'paymentMethod', 'created_at'];

	resultsLength2: number;
	@ViewChild('sort2') sort2: MatSort;
	@ViewChild('paginator2') paginator2: MatPaginator;
	public dataSource2 = new MatTableDataSource();
	displayedColumns2: string[] = ['firstName', 'point', 'tshirt', 'cabin'];


	resultsLength3: number;
	@ViewChild('sort3') sort3: MatSort;
	@ViewChild('paginator3') paginator3: MatPaginator;
	public dataSource3 = new MatTableDataSource();
	displayedColumns3: string[] = ['firstName', 'team', 'subteam', 'sport'];

	resultsLength4: number;
	@ViewChild('sort4') sort4: MatSort;
	@ViewChild('paginator4') paginator4: MatPaginator;
	public dataSource4 = new MatTableDataSource();
	displayedColumns4: string[] = ['firstName', 'medicalIssues', 'foodAllergy', 'ongoingMedication'];

	public dataSource5 = new MatTableDataSource();
	displayedColumns5: string[] = ['xs', 's', 'm', 'l', 'xl'];

	public dataSource6 = new MatTableDataSource();
	displayedColumns6: string[] = ['point', 'count'];

	headers: Headers = new Headers();
	recoptions: any;
	constructor(public snackBar: MatSnackBar,
		public sportsServ: SportsService,
		public httpClient: HttpClient,
		private layoutConfigService: LayoutConfigService, private booking: BookingService,
		public evnt: EventService, public home: HomeService, private mp: MeetingpointsService, public reports: ReportsService, public router: Router, private activatedRoute: ActivatedRoute) {
		this.headers.append('enctype', 'multipart/form-data');
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('X-Requested-With', 'XMLHttpRequest');
		// this.headers.append('Access-Control-Allow-Origin','*');

		this.recoptions = new RequestOptions({ headers: this.headers });
		this.stateCtrl = new FormControl();
	}

	ngOnInit() {
		this.home.getYears().subscribe((data) => {
			this.years = data;
			// console.log(data)
			this.yearValue = this.years[0].year;
		});
		this.home.getnextCamp().subscribe((res) => {
			// console.log(res)
			this.home.getCamps(this.yearValue).subscribe((data) => {
				this.campID = data[0].id;

				this.campValue = data[0].id;
				this.camps = data;
				let temp = this.camps.filter(item => item.id == this.campValue)
				if (temp && temp.length > 0)
					this.campType = temp[0].type;
				this.statsEvents(this.campID);
				this.getTimeline(this.campID);
			});


		});


		this.chartOptions1 = {
			data: [10, 14, 18, 11, 9, 12, 14, 17, 18, 14],
			color: this.layoutConfigService.getConfig('colors.state.brand'),
			border: 3
		};
		this.chartOptions2 = {
			data: [11, 12, 18, 13, 11, 12, 15, 13, 19, 15],
			color: this.layoutConfigService.getConfig('colors.state.danger'),
			border: 3
		};
		this.chartOptions3 = {
			data: [12, 12, 18, 11, 15, 12, 13, 16, 11, 18],
			color: this.layoutConfigService.getConfig('colors.state.success'),
			border: 3
		};
		this.chartOptions4 = {
			data: [11, 9, 13, 18, 13, 15, 14, 13, 18, 15],
			color: this.layoutConfigService.getConfig('colors.state.warning'),
			border: 3
		};
	}

	statsEvents(id: number) {
		this.home.sports(id).subscribe((data) => {

			this.dataSource3.data = data;
			this.resultsLength3 = data.length;
			this.dataSource3.sort = this.sort3;
			this.dataSource3.paginator = this.paginator3;
			// console.log(this.dataSource3)
		});
		this.home.getYears().subscribe((data) => {
			this.years = data;
		});

		this.home.getGeneralStats(id).subscribe((data) => {
			this.items = data;
			// console.log(this.items)
		});

		this.home.bookingData(id).subscribe((data) => {
			this.dataSource1.data = data;
			this.resultsLength1 = data.length;
			this.dataSource1.sort = this.sort1;
			this.dataSource1.paginator = this.paginator;
		});

		this.home.campersData(id).subscribe((data) => {
			this.dataSource2.data = data;
			this.resultsLength2 = data.length;
			this.dataSource2.sort = this.sort2;
			this.dataSource2.paginator = this.paginator2;
		});



		this.home.medicalConditions(id).subscribe((data) => {
			this.dataSource4.data = data;
			this.resultsLength4 = data.length;
			this.dataSource4.sort = this.sort4;
			this.dataSource4.paginator = this.paginator4;
		});

		this.home.tshirts(id).subscribe((data) => {
			this.dataSource5.data = data;
		});

		this.home.meetingPoints(id).subscribe((data) => {
			this.dataSource6.data = data;
		});

		this.mp.list().subscribe(data => {
			this.meetingpoints = data;
		});
		this.sportsServ.list().subscribe(data => {
			this.sports = data;
		});
	}

	getItemCssClass(team: string): string {
		switch (team) {
			case 'Waiting List':
				return 'danger';
			case 'Finished':
				return 'success';
			case 'Refunded':
				return 'danger';
			case 'Done':
				return 'success';
			case 'Alpha':
				return 'danger';
			case 'Omega':
				return 'primary';
			case 'Male':
				return 'info';
			case 'Female':
				return 'danger';
		}
		return '';
	}



	edit(id: number) {
		this.router.navigate(['../booking/edit', id], { relativeTo: this.activatedRoute });
	}

	editProfile(id: number) {
		this.router.navigate(['../kids/view', id], { relativeTo: this.activatedRoute });
	}

	selectChangeHandler() {
		this.home.getCamps(this.yearValue).subscribe((data) => {
			this.camps = data;
			let temp = this.camps.filter(item => item.id == this.campValue)
			if (temp && temp.length > 0)
				this.campType = temp[0].type;
		});
	}
	onAlertClose(evt) {

	}

	getCampData() {
		this.campID = this.campValue;
		// console.log(this.camps)
		this.statsEvents(this.campValue);
		this.getTimeline(this.campValue);
		let temp = this.camps.filter(item => item.id == this.campValue)
		if (temp && temp.length > 0)
			this.campType = temp[0].type;
		// console.log(this.campType)
	}

	GenerateDistribution() {
		this.httpClient.get('https://kidsgamescamp.com/api/report/' + this.campValue, this.recoptions).subscribe(res => {
			// console.log(res)
			if (res["status"] == true)
				alert("Report is ready to be downloaded .")
		})
	}



	getTimeline(id: number) {
		this.home.lastBooking(id).subscribe((data) => {
			this.lastbooking = data;
		});

	}

	calculate(no1, no2) {
		return parseInt(no1) + parseInt(no2);
	}

	calculateMinus(no1, no2) {
		return parseInt(no1) - parseInt(no2);
	}

	applyFilter(filterValue: string) {
		this.dataSource1.filter = filterValue.trim().toLowerCase();
	}

	applyFilter2(filterValue: string) {
		this.dataSource2.filter = filterValue.trim().toLowerCase();
	}

	applyFilter3(filterValue: string) {
		this.dataSource3.filter = filterValue.trim().toLowerCase();
	}

	applyFilter4(filterValue: string) {
		this.dataSource4.filter = filterValue.trim().toLowerCase();
	}

	applyFilter5(filterValue: string) {
		this.dataSource5.filter = filterValue.trim().toLowerCase();
	}

	applyFilter6(filterValue: string) {
		this.dataSource6.filter = filterValue.trim().toLowerCase();
	}

	// Reports

}