import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SportsService } from '../../../../service/sports.service';
import { Sports } from  '../../../../sports';
import { ActivatedRoute, Router } from '@angular/router';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
	parse: {
	  dateInput: 'LL',
	},
	display: {
	  dateInput: 'DD-MM-YYYY',
	  monthYearLabel: 'YYYY',
	  dateA11yLabel: 'LL',
	  monthYearA11yLabel: 'YYYY',
	},
  };

@Component({
  selector: 'kt-sport-create',
  templateUrl: './coupons-create.component.html',
  styleUrls: ['./coupons-create.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class CouponsCreateComponent implements OnInit {

  Sports: Sports[]=[];
  genderOptions = [
		{ key: 'Male', value: "Male" },
		{ key: 'Female', value: "Female" },
		{ key: 'Mix', value: "Mix" }
    ];
    
	constructor(public location : Location,public snackBar: MatSnackBar,  public service: SportsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

  ngOnInit() {  }

	onSubmit(form){

		const obj = {
			name:  form.value.name,
			 gender:  form.value.gender,
			 seatsLimit:  form.value.seatsLimit,
			// ageFrom:  form.value.ageFrom,
			// ageTo:  form.value.ageTo
			};
			this.service.create(obj).subscribe((data)=>{
			});

			setTimeout(() => {
				this.cancel();
			}, 1000);
		}

		cancel(){
			this.location.back();
			return;
			this.router.navigate(['/default/sports'], { relativeTo: this.activatedRoute });
		}
}
