import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { CouponsService } from '../../../service/coupons.service';
import { Coupons } from  '../../../coupons';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-sports',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.scss']
})
export class CouponsComponent implements OnInit {

	coupons: Coupons[]=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
  displayedColumns1: string[] = ['code','value','Guardian','actions'];
  
	constructor(public snackBar: MatSnackBar,  public service: CouponsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.list().subscribe((data)=>{
			this.coupons = data;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
    // console.log(id);
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
	}

	delete(id){
		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.service.delete(id).subscribe((data)=>{});
        }else{ return false;}
		setTimeout(() => {
			this.ngOnInit();
        }, 1000);
	}
}