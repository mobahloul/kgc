import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CouponsService } from '../../../../service/coupons.service';
import { Coupons } from '../../../../coupons';
import { ActivatedRoute, Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
	parse: {
		dateInput: 'LL',
	},
	display: {
		dateInput: 'DD-MM-YYYY',
		monthYearLabel: 'YYYY',
		dateA11yLabel: 'LL',
		monthYearA11yLabel: 'YYYY',
	},
};

@Component({
	selector: 'kt-sport-edit',
	templateUrl: './coupon-edit.component.html',
	styleUrls: ['./coupon-edit.component.scss'],
	providers: [
		{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
		{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
	],
})
export class CouponseditComponent implements OnInit {

	items: Coupons[] = [];
	id = null;

	genderOptions = [
		{ key: 'Male', value: "Male" },
		{ key: 'Female', value: "Female" },
		{ key: 'Mix', value: "Mix" }
	];

	constructor(public location: Location, public snackBar: MatSnackBar, public service: CouponsService, public router: Router, private activatedRoute: ActivatedRoute) {
		this.id = this.activatedRoute.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.service.get(this.id).subscribe((data) => {
			this.items = data;
		});
	}

	onSubmit(form) {

		const obj = {
			name: form.value.name,
			gender: form.value.gender,
			seatsLimit: form.value.seatsLimit,
			// ageFrom:  form.value.ageFrom,
			// ageTo:  form.value.ageTo
		};

		this.service.edit(this.id, obj).subscribe((data) => {
		});
		setTimeout(() => {
			this.cancel();
		}, 1000);
	}

	cancel() {
		this.location.back();
		return;
		this.router.navigate(['/default/sports'], { relativeTo: this.activatedRoute });
	}

}
