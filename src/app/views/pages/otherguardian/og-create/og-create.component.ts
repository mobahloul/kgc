import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { OtherguardiansService } from '../../../../service/otherguardians.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Otherguardians } from '../../../../otherguardians';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-og-create',
  templateUrl: './og-create.component.html',
  styleUrls: ['./og-create.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class OgCreateComponent implements OnInit {

  firstName = null;
  middleName = null;
  familyName = null;
  email = null;
  mobile = null;
  birthDate = null;
  IDType = null;
  IDNumber = null;
  profileImage = null;
  items: Otherguardians[] = [];
  familyID = null;

  options = [
    { key: 'National ID', value: "National ID" },
    { key: 'Passport', value: "Passport" }
  ];

  genders = [
    { key: 'Male', value: "Male" },
    { key: 'Female', value: "Female" }
  ];

  relations = [
    { key: 'Father', value: "Father" },
    { key: 'Mother', value: "Mother" },
    { key: 'Grandfather', value: "Grandfather" },
    { key: 'Grandmother', value: "Grandmother" },
    { key: 'Uncle', value: "Uncle" },
    { key: 'Aunt', value: "Aunt" }
  ];

  constructor(public location: Location, public snackBar: MatSnackBar, public service: OtherguardiansService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.familyID = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
  }

  onSubmit(form) {

    const object = {
      familyID: this.familyID,
      firstName: form.value.firstName,
      middleName: form.value.middleName,
      familyName: form.value.familyName,
      email: form.value.email,
      mobile: form.value.mobile,
      birthDate: form.value.birthDate,
      IDType: form.value.IDType,
      IDNumber: form.value.IDNumber,
    };

    this.service.create(object).subscribe((data) => {
    });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/guardians'], { relativeTo: this.activatedRoute });
  }
}
