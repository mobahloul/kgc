import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OgCreateComponent } from './og-create.component';

describe('OgCreateComponent', () => {
  let component: OgCreateComponent;
  let fixture: ComponentFixture<OgCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OgCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OgCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
