import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OgViewComponent } from './og-view.component';

describe('OgViewComponent', () => {
  let component: OgViewComponent;
  let fixture: ComponentFixture<OgViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OgViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OgViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
