import { Component, OnInit, Input,ChangeDetectionStrategy } from '@angular/core';
import { OtherguardiansService } from '../../../../service/otherguardians.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Guardians } from  '../../../../guardians';
import {Location} from '@angular/common';

@Component({
  selector: 'kt-og-view',
  templateUrl: './og-view.component.html',
  styleUrls: ['./og-view.component.scss']
})
export class OgViewComponent implements OnInit {

  items:any;
  id=null;

  constructor(public service: OtherguardiansService, private location: Location,
    public router: Router,private activatedRoute: ActivatedRoute) {
		this.id = this.activatedRoute.snapshot.paramMap.get('id');	
	}

  ngOnInit() {

    this.service.get(this.id).subscribe((data)=>{
			this.items = data;
    });
    
  }

	edit(id){
     this.router.navigate(['/default/otherguardian/edit', id], { relativeTo: this.activatedRoute });
  }
   
  cancel(){
    this.location.back();
  }

}
