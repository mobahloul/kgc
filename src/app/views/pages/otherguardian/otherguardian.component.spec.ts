import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherguardianComponent } from './otherguardian.component';

describe('OtherguardianComponent', () => {
  let component: OtherguardianComponent;
  let fixture: ComponentFixture<OtherguardianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherguardianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherguardianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
