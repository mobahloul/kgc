import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { OtherguardiansService } from '../../../../service/otherguardians.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Otherguardians } from '../../../../otherguardians';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-og-edit',
  templateUrl: './og-edit.component.html',
  styleUrls: ['./og-edit.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class OgEditComponent implements OnInit {

  items: Otherguardians[] = [];
  id = null;


  constructor(public snackBar: MatSnackBar, public service: OtherguardiansService,
    public location: Location,
    public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.service.get(this.id).subscribe((data) => {
      this.items = data;
    });
  }

  onSubmit(form) {

    const obj = {
      firstName: form.value.firstName,
      middleName: form.value.middleName,
      familyName: form.value.familyName,
      email: form.value.email,
      mobile: form.value.mobile,
    };

    this.service.edit(this.id, obj).subscribe((data) => {
    });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/guardians'], { relativeTo: this.activatedRoute });
  }
}
