import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OgEditComponent } from './og-edit.component';

describe('OgEditComponent', () => {
  let component: OgEditComponent;
  let fixture: ComponentFixture<OgEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OgEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OgEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
