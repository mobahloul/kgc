import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { StoreService } from '../../../../service/store.service';
import { Store } from '../../../../store';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
	selector: 'kt-store-edit',
	templateUrl: './store-edit.component.html',
	styleUrls: ['./store-edit.component.scss']
})
export class StoreEditComponent implements OnInit {
	id = null;

	items: Store[] = [];
	statusOptions = [
		{ key: 'Active', value: "Active" },
		{ key: 'Inactive', value: "Inactive" }
	];

	constructor(public location: Location, public snackBar: MatSnackBar, public service: StoreService, public router: Router, private activatedRoute: ActivatedRoute) {
		this.id = this.activatedRoute.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.service.get(this.id).subscribe((data) => {
			this.items = data;
		});
	}

	onSubmit(form) {

		const obj = {
			productName: form.value.productName,
			productDescription: form.value.productDescription,
			price: form.value.price,
			amount: form.value.amount,
			status: form.value.status,
			image: form.value.image
		};
		this.service.edit(this.id, obj).subscribe((data) => {
		});

		setTimeout(() => {
			this.cancel();
		}, 1000);
	}

	cancel() {
		this.location.back();
		return;
		this.router.navigate(['/default/store'], { relativeTo: this.activatedRoute });
	}
}
