import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { StoreService } from '../../../../service/store.service';
import { Store } from '../../../../store';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
	selector: 'kt-store-create',
	templateUrl: './store-create.component.html',
	styleUrls: ['./store-create.component.scss']
})
export class StoreCreateComponent implements OnInit {

	Store: Store[] = [];
	productName = null;
	productDescription = null;
	price = null;
	amount = null;
	status = null;
	image = null;
	statusOptions = [
		{ key: 'Active', value: "Active" },
		{ key: 'Inactive', value: "Inactive" }
	];

	constructor(public location: Location, public snackBar: MatSnackBar, public service: StoreService, public router: Router, private activatedRoute: ActivatedRoute) { }

	ngOnInit() { }

	onSubmit(form) {

		const obj = {
			productName: form.value.productName,
			productDescription: form.value.productDescription,
			price: form.value.price,
			amount: form.value.amount,
			status: form.value.status,
			image: form.value.image
		};
		this.service.create(obj).subscribe((data) => {
		});

		setTimeout(() => {
			this.cancel();
		}, 1000);
	}

	cancel() {
		this.location.back();
		return;
		this.router.navigate(['/default/store'], { relativeTo: this.activatedRoute });
	}
}
