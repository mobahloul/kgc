import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GurdiansComponent } from './gurdians.component';

describe('GurdiansComponent', () => {
  let component: GurdiansComponent;
  let fixture: ComponentFixture<GurdiansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GurdiansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GurdiansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
