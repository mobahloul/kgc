import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { GuardiansService } from '../../../../service/guardians.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Guardians } from '../../../../guardians';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-guard-create',
  templateUrl: './guard-create.component.html',
  styleUrls: ['./guard-create.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class GuardCreateComponent implements OnInit {
  firstName = null;
  middleName = null;
  familyName = null;
  gender = null;
  email = null;
  mobile = null;
  birthDate = null;
  IDType = null;
  IDNumber = null;
  job = null;
  employer = null;
  profileImage = null;
  items: Guardians[] = [];
  id = null;

  options = [
    { key: 'National ID', value: "National ID" },
    { key: 'Passport', value: "Passport" }
  ];

  genders = [
    { key: 'Male', value: "Male" },
    { key: 'Female', value: "Female" }
  ];

  constructor(public location: Location, public snackBar: MatSnackBar, public service: GuardiansService, public router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() { }

  onSubmit(form) {

    const object = {
      firstName: form.value.firstName,
      middleName: form.value.middleName,
      familyName: form.value.familyName,
      gender: form.value.gender,
      email: form.value.email,
      mobile: form.value.mobile,
      birthDate: form.value.birthDate,
      IDType: form.value.IDType,
      IDNumber: form.value.IDNumber,
      job: form.value.job,
      employer: form.value.employer
    };

    this.service.create(object).subscribe((data) => { });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/guardians'], { relativeTo: this.activatedRoute });
  }
}
