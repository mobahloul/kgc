import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardCreateComponent } from './guard-create.component';

describe('GuardCreateComponent', () => {
  let component: GuardCreateComponent;
  let fixture: ComponentFixture<GuardCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
