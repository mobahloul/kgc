import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { GuardiansService } from '../../../../service/guardians.service';
import { OtherguardiansService } from '../../../../service/otherguardians.service';
import { KidsService } from '../../../../service/kids.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Guardians } from '../../../../guardians';
import { BookingService } from '../../../../service/booking.service';
import { Location } from '@angular/common';
@Component({
  selector: 'kt-guard-view',
  templateUrl: './guard-view.component.html',
  styleUrls: ['./guard-view.component.scss']
})
export class GuardViewComponent implements OnInit {

  items: Guardians[] = [];
  kids: Guardians[] = [];
  guardians: Guardians[] = [];
  camps: any;
  guardian: any;
  id = null;

  constructor(public location: Location, public service: GuardiansService, public Otherguardians: OtherguardiansService, private booking: BookingService,
    public KidsService: KidsService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {

    this.service.get(this.id).subscribe((data) => {
      this.items = data;
    });

    this.service.getOtherGuardian(this.id).subscribe((data) => {
      this.guardians = data;
    });

    this.service.get(this.id).subscribe((data) => {
      this.guardian = data;
    });

    this.service.getKids(this.id).subscribe((data) => {
      this.kids = data;
    });

    this.booking.getCamps(this.id).subscribe((data) => {
      this.camps = data;
    });

  }

  edit(id) {
    this.router.navigate(['../../edit', id], { relativeTo: this.activatedRoute });
  }

  add_guard(id) {
    this.router.navigate(['../../../otherguardian/create', id], { relativeTo: this.activatedRoute });
  }

  view_guard(id: number) {
    this.router.navigate(['../../../otherguardian/view', id], { relativeTo: this.activatedRoute });
  }

  edit_guard(id: number) {
    this.router.navigate(['../../../otherguardian/edit', id], { relativeTo: this.activatedRoute });
  }

  delete_guard(id) {
    this.Otherguardians.delete(id).subscribe((data) => {
      // console.log(data);
    });

    setTimeout(() => {
      this.ngOnInit();
    }, 1000);
  }

  add_kid(id) {
    this.router.navigate(['../../../kids/create', id], { relativeTo: this.activatedRoute });
  }

  view_kid(id: number) {
    this.router.navigate(['../../../kids/view', id], { relativeTo: this.activatedRoute });
  }

  edit_kid(id) {
    this.router.navigate(['../../../kids/edit', id], { relativeTo: this.activatedRoute });
  }

  delete_kid(id) {
    this.KidsService.delete(id).subscribe((data) => {
    });

    setTimeout(() => {
      this.ngOnInit();
    }, 1000);
  }


  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/guardians'], { relativeTo: this.activatedRoute });
  }

  saveComment(form) {
    const object = {
      comments: form.value.comments,
    };

    this.service.saveComment(this.id, object).subscribe((data) => { });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }
  onSubmit(f, id) {
    const object = {
      couponval: f.value.discount,
      couponcode: f.value.promocode,
      couponreason: f.value.couponReson,
    };
    this.service.saveCoupon(this.id, object).subscribe((data) => { });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

}
