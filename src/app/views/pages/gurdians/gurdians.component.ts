import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { GuardiansService } from '../../../service/guardians.service';
import { Guardians } from  '../../../guardians';
import { ActivatedRoute, Router } from '@angular/router';

import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'kt-gurdians',
  templateUrl: './gurdians.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./gurdians.component.scss']
})
export class GurdiansComponent implements OnInit {
	Guardians: Guardians[]=[];
	selection = new SelectionModel(true, []);
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 

	displayedColumns1: string[] = ['select','firstName', 'email', 'mobile', 'job','actions'];

	constructor(public snackBar: MatSnackBar,  private service: GuardiansService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	
	ngOnInit() { 
		this.get_all();
		console.log(this.router.url);
	}

	get_all(){
		this.service.list().subscribe((data)=>{
			this.Guardians = data;
			this.resultsLength = data.length;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	view(id: number){
		this.router.navigate(['view', id], { relativeTo: this.activatedRoute });
	}

	edit(id: number){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
	}

	delete(id){
		var r=confirm("Do you want to delete this item?");
        if (r==true){
			this.service.delete(id).subscribe((data)=>{});
		}else{ return false;}
		
		setTimeout(() => {
			this.ngOnInit();
			}, 1000);
	}

	//selection
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.Guardians.length;
		return numSelected === numRows;
	}

	/**
	 * Selects all rows if they are not all selected; otherwise clear selection
	 */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.Guardians.forEach(row => this.selection.select(row));
		}
	}

send(){}

}
