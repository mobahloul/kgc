import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { GuardiansService } from '../../../../service/guardians.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Guardians } from '../../../../guardians';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'kt-guard-edit',
  templateUrl: './guard-edit.component.html',
  styleUrls: ['./guard-edit.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class GuardEditComponent implements OnInit {

  items: Guardians[] = [];
  id = null;

  options = [
    { key: 'National ID', value: "National ID" },
    { key: 'Passport', value: "Passport" }
  ];

  genders = [
    { key: 'Male', value: "Male" },
    { key: 'Female', value: "Female" }
  ];

  constructor(public location: Location, public snackBar: MatSnackBar, public service: GuardiansService, public router: Router, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.service.get(this.id).subscribe((data) => {
      this.items = data;
    });
  }

  onSubmit(form) {

    const obj = {
      firstName: form.value.firstName,
      middleName: form.value.middleName,
      familyName: form.value.familyName,
      gender: form.value.gender,
      email: form.value.email,
      mobile: form.value.mobile,
      birthDate: form.value.birthDate,
      IDType: form.value.IDType,
      IDNumber: form.value.IDNumber,
      job: form.value.job,
      employer: form.value.employer,
    };

    this.service.edit(this.id, obj).subscribe((data) => { });

    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  pwdUpdate(form) {
    const obj = {
      password: form.value.password,
    };

    this.service.pwdUpdate(this.id, obj).subscribe((data) => { });
    setTimeout(() => {
      this.cancel();
    }, 1000);
  }

  cancel() {
    this.location.back();
    return;
    this.router.navigate(['/default/guardians'], { relativeTo: this.activatedRoute });
  }
}
