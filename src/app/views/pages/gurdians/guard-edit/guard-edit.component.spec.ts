import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardEditComponent } from './guard-edit.component';

describe('GuardEditComponent', () => {
  let component: GuardEditComponent;
  let fixture: ComponentFixture<GuardEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
