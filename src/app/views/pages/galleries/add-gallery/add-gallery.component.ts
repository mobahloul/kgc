import { Component, OnInit, Renderer2, ViewChild, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { GalleriesService } from '../../../../service/galleries.service';
import { Galleries } from '../../../../galleries';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpEventType } from '@angular/common/http';
import { Location } from '@angular/common';
@Component({
    selector: 'kt-add-gallery',
    templateUrl: './add-gallery.component.html',
    styleUrls: ['./add-gallery.component.scss']
})
export class AddGalleryComponent implements OnInit {
    @ViewChild('uploadImages') private image: ElementRef;
    postTitle: string;
    url: string;
    galleryCover;
    galleryTitle: any;
    images = [];
    increasevals: number = 0;
    showProgress: boolean = false;
    constructor(public location : Location,public snackBar: MatSnackBar, public service: GalleriesService, public router: Router, private activatedRoute: ActivatedRoute, private renderer: Renderer2, private ref: ChangeDetectorRef) {
    }

    ngOnInit() {



    }

    onSubmit(f) {
        let formdata = new FormData();
        formdata.append('title', this.galleryTitle);
        formdata.append('image', this.galleryCover);
        for (var i = 0; i < this.images.length; i++) {
            formdata.append("galleryImages[]", this.images[i]);
        }
        this.showProgress = true;
        this.service.create(formdata).subscribe((events) => {
            if (events.type == HttpEventType.UploadProgress) {

                this.increasevals = Math.round(events.loaded / events.total * 100);
                this.ref.detectChanges();



            } else if (events.type === HttpEventType.Response) {
                setTimeout(() => {
                    this.cancel();
                }, 1000);
            }
            //       if(data){
            //          setTimeout(() => {
            //				this.cancel();
            //			}, 1000);
            //       }
        }, (error) => {
            // console.log(error);
        });
        // console.log(formdata);
    }
    onSelectFile(fileList): void {
        let file = fileList[0];
        let fileReader: FileReader = new FileReader();
        let self = this;
        let imageurl;
        self.galleryCover = file;
        fileReader.onload = (e: any) => {

            (<HTMLImageElement>document.getElementById('image')).src = e.target.result as string;

        }
        fileReader.readAsDataURL(file);
    }
    galleryImagesChange(event) {
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                this.images.push(event.target.files[i]);
                var reader = new FileReader();

                reader.onload = (event: any) => {

                    const img: HTMLImageElement = this.renderer.createElement('img');
                    img.src = event.target.result;
                    this.renderer.addClass(img, 'imageUploaded');
                    this.renderer.appendChild(this.image.nativeElement, img);



                }

                reader.readAsDataURL(event.target.files[i]);
            }
        }

    }
    cancel() {
        this.location.back();
        return;
        this.router.navigate(['/default/galleries'], { relativeTo: this.activatedRoute });
    }

}
