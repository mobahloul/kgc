import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { GalleriesAlbumsService } from '../../../service/galleries_albums.service';
import { Galleries_albums } from  '../../../galleries_albums';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-galleries',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {

	items: any=[];
	resultsLength:number;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	public dataSource = new MatTableDataSource();	 
  displayedColumns1: string[] = ['title','date','status','actions'];
  
	constructor(public snackBar: MatSnackBar,  public service: GalleriesAlbumsService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() { 
		this.get_all();
	}

	get_all(){
		this.service.list().subscribe((data)=>{
			this.items = data;
			this.dataSource.data = data;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  }
	  
	add(){
		this.router.navigate(['create'], { relativeTo: this.activatedRoute });
	}

	edit(id){
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
	}

	delete(id){
		this.service.delete(id).subscribe((data)=>{
		});
		
		setTimeout(() => {
			this.ngOnInit();
		}, 1000);
		
	}
}