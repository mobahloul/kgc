import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { GalleriesService } from '../../../../service/galleries.service';
import { GalleriesAlbumsService } from '../../../../service/galleries_albums.service';
import { Galleries } from  '../../../../galleries';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'kt-add-gallery',
  templateUrl: './add-album.component.html',
  styleUrls: ['./add-album.component.scss']
})
export class AddAlbumComponent implements OnInit {
galleries:Galleries[];
Albumimage;
gallery;
  constructor(public location : Location,public snackBar: MatSnackBar, public GalleriesAlbumsSe : GalleriesAlbumsService, public Gservice: GalleriesService, public router: Router,private activatedRoute: ActivatedRoute) {
	}

  ngOnInit() {
     this.Gservice.list().subscribe((data)=>{
         this.galleries = data;
     });
  }
  onSubmit(f){
      let formdata = new FormData();
      formdata.append('gallery',this.gallery);
      formdata.append('image',this.Albumimage);
      this.GalleriesAlbumsSe.create(formdata).subscribe((data)=>{
       if(data){
          setTimeout(() => {
				this.cancel();
			}, 1000);
       }
      },(error)=>{
    //  console.log(error);
      });
      //console.log(formdata);
  }
  onFileChange(event) {
      //console.log(event);
       var reader = new FileReader();
   this.Albumimage = event.target.files[0];
                reader.onload = (event:any) => {
                }
  
                reader.readAsDataURL(event.target.files[0]);
  }
cancel(){
  this.location.back();
  return;
			this.router.navigate(['/default/albums'], { relativeTo: this.activatedRoute });
		}

}
