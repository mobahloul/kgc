
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { GalleriesService } from '../../../../service/galleries.service';
import { GalleriesAlbumsService } from '../../../../service/galleries_albums.service';
import { Galleries } from  '../../../../galleries';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'kt-edit-gallery',
  templateUrl: './edit-album.component.html',
  styleUrls: ['./edit-album.component.scss']
})
export class EditAlbumComponent implements OnInit {
galleries:Galleries[];
Albumimage;
gallery;
id;
url:string;
  constructor(public location : Location,public snackBar: MatSnackBar, public GalleriesAlbumsSe : GalleriesAlbumsService, public Gservice: GalleriesService, public router: Router,private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
	}

  ngOnInit() {
    this.GalleriesAlbumsSe.get(this.id).subscribe((data)=>{
		this.gallery= data.gallery;
		this.url= data.image;
    });
     this.Gservice.list().subscribe((data)=>{
         this.galleries = data;
     });
  }
  onSubmit(f){
      let formdata = new FormData();
      formdata.append('gallery',this.gallery);
      formdata.append('image',this.Albumimage);
      this.GalleriesAlbumsSe.edit(this.id,formdata).subscribe((data)=>{
       if(data){
          setTimeout(() => {
				this.cancel();
			}, 1000);
       }
      },(error)=>{
    //  console.log(error);
      });
      //console.log(formdata);
  }
  onFileChange(event) {
      //console.log(event);
       var reader = new FileReader();
   this.Albumimage = event.target.files[0];
                reader.onload = (event:any) => {
                }
  
                reader.readAsDataURL(event.target.files[0]);
  }
cancel(){
  this.location.back();
  return;
			this.router.navigate(['/default/albums'], { relativeTo: this.activatedRoute });
		}

}

