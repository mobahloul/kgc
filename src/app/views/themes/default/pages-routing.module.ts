// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../../core/auth';
import { NgxPermissionsGuard } from 'ngx-permissions';
// My Component

import { EventsComponent } from './../../pages/events/events.component';
import { CreateComponent } from './../../pages/events/create/create.component';
import { EditComponent } from './../../pages/events/edit/edit.component';

import { GurdiansComponent } from './../../pages/gurdians/gurdians.component';
import { GuardEditComponent } from './../../pages/gurdians/guard-edit/guard-edit.component';
import { GuardCreateComponent } from './../../pages/gurdians/guard-create/guard-create.component';
import { GuardViewComponent } from './../../pages/gurdians/guard-view/guard-view.component';

import { MeetingPointsComponent } from './../../pages/meeting-points/meeting-points.component';
import { PointCreateComponent } from './../../pages/meeting-points/point-create/point-create.component';
import { PointEditComponent } from './../../pages/meeting-points/point-edit/point-edit.component';

import { KidsComponent } from './../../pages/kids/kids.component';
import { KidEditComponent } from './../../pages/kids/kid-edit/kid-edit.component';
import { KidCreateComponent } from './../../pages/kids/kid-create/kid-create.component';
import { KidViewComponent } from './../../pages/kids/kid-view/kid-view.component';

import { AdminsComponent } from './../../pages/admins/admins.component';
import { AdminCreateComponent } from './../../pages/admins/admin-create/admin-create.component';
import { AdminEditComponent } from './../../pages/admins/admin-edit/admin-edit.component';


import { BookingComponent } from './../../pages/booking/booking.component';
import { BookingCreateComponent } from './../../pages/booking/booking-create/booking-create.component';
import { BookingEditComponent } from './../../pages/booking/booking-edit/booking-edit.component';

import { TeamsComponent } from './../../pages/teams/teams.component';
import { ReservationComponent } from './../../pages/teams/reservation/reservation.component';

//reports
import { EventsreportComponent } from './../../pages/reports/eventsreport/eventsreport.component';
import { TeamsreportComponent } from './../../pages/reports/teamsreport/teamsreport.component';
import { TeamsDetailsComponent } from './../../pages/reports/teamsreport/teams-details/teams-details.component';
import { CabinsReportsComponent } from './../../pages/cabins-reports/cabins-reports.component';
import { CabinDetailsComponent } from './../../pages/cabins-reports/cabin-details/cabin-details.component';
import { HomeComponent } from './../../pages/home/home.component';

import { PaymentComponent } from './../../pages/reports/payment/payment.component';
import { BookingreportComponent } from './../../pages/reports/bookingreport/bookingreport.component';
import { GenderComponent } from './../../pages/reports/gender/gender.component';

import { SportsComponent } from './../../pages/sports/sports.component';
import { SportCreateComponent } from './../../pages/sports/sport-create/sport-create.component';
import { SportEditComponent } from './../../pages/sports/sport-edit/sport-edit.component';


import { StoreComponent } from './../../pages/store/store.component';
import { StoreCreateComponent } from './../../pages/store/store-create/store-create.component';
import { StoreEditComponent } from './../../pages/store/store-edit/store-edit.component';

import { FamilyeventsComponent } from './../../pages/familyevents/familyevents.component';
import { FeAddComponent } from './../../pages/familyevents/fe-add/fe-add.component';
import { FeEditComponent } from './../../pages/familyevents/fe-edit/fe-edit.component';
import { FamilybookingComponent } from './../../pages/familybooking/familybooking.component';
import { FbEditComponent } from './../../pages/familybooking/fb-edit/fb-edit.component';

import { OrdersComponent } from './../../pages/orders/orders.component';
import { OrdersCreateComponent } from './../../pages/orders/orders-create/orders-create.component';
import { OrdersViewComponent } from './../../pages/orders/orders-view/orders-view.component';
import { OrdersEditComponent } from './../../pages/orders/orders-edit/orders-edit.component';

import { OtherguardianComponent } from './../../pages/otherguardian/otherguardian.component';
import { OgCreateComponent } from './../../pages/otherguardian/og-create/og-create.component';
import { OgEditComponent } from './../../pages/otherguardian/og-edit/og-edit.component';
import { OgViewComponent } from '../../pages/otherguardian/og-view/og-view.component';

import { GalleriesComponent } from '../../pages/galleries/galleries.component';
import { AddGalleryComponent } from '../../pages/galleries/add-gallery/add-gallery.component';
import { EditGalleryComponent } from '../../pages/galleries/edit-gallery/edit-gallery.component';
import { NewsComponent } from '../../pages/news/news.component';
import { AddNewsComponent } from '../../pages/news/add-news/add-news.component';
import { EditNewsComponent } from '../../pages/news/edit-news/edit-news.component';
import { CouponsComponent } from '../../pages/coupons/coupons.component';
import { CouponseditComponent } from '../../pages/coupons/coupons-edit/coupon-edit.component';

import { AlbumsComponent } from '../../pages/albums/albums.component';
import { AddAlbumComponent } from '../../pages/albums/add-album/add-album.component';
import { EditAlbumComponent } from '../../pages/albums/edit-album/edit-album.component';

const routes: Routes = [
	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'dashboard',
				// loadChildren: 'app/views/pages/dashboard/dashboard.module#DashboardModule'
				component: HomeComponent 
			},
			{
				path: 'home',
				component: HomeComponent 
			},
			{
				path: 'events', // <= Page URL
				component: EventsComponent // <= Page component registration
			   },{
               path: 'Coupons', // <= Page URL
				component: CouponsComponent // <= Page component registration
			   }
               ,{
               path: 'Coupons/edit/:id', // <= Page URL
				component: CouponseditComponent // <= Page component registration
			   },
               
			   {
				path: 'events/create', // <= Page URL
				component: CreateComponent // <= Page component registration
			   },
			   {
				path: 'events/edit/:id', // <= Page URL
				component: EditComponent // <= Page component registration
			   },
			   {
			   path: 'family-events', // <= Page URL
			   component: FamilyeventsComponent // <= Page component registration
			  },
			  {
			   path: 'family-events/create', // <= Page URL
			   component: FeAddComponent // <= Page component registration
			  },
			  {
			   path: 'family-events/edit/:id', // <= Page URL
			   component: FeEditComponent // <= Page component registration
			  },
			  {
				path: 'family-booking', // <= Page URL
				component: FamilybookingComponent // <= Page component registration
			   },
			   {
				path: 'family-booking/edit/:id', // <= Page URL
				component: FbEditComponent // <= Page component registration
			   },
			   {
				path: 'meeting-points', // <= Page URL
				component: MeetingPointsComponent // <= Page component registration
			   },
			   {
				path: 'meeting-points/create', // <= Page URL
				component: PointCreateComponent // <= Page component registration
			   },
			   {
				path: 'meeting-points/edit/:id', // <= Page URL
				component: PointEditComponent // <= Page component registration
		   	},

			   {
				path: 'sports', // <= Page URL
				component: SportsComponent // <= Page component registration
			   },
			   {
				path: 'sports/create', // <= Page URL
				component: SportCreateComponent // <= Page component registration
			   },
			   {
				path: 'sports/edit/:id', // <= Page URL
				component: SportEditComponent // <= Page component registration
			   },
			   
			   {
				path: 'store', // <= Page URL
				component: StoreComponent // <= Page component registration
			   },
			   {
				path: 'store/create', // <= Page URL
				component: StoreCreateComponent // <= Page component registration
			   },
			   {
				path: 'store/edit/:id', // <= Page URL
				component: StoreEditComponent // <= Page component registration
			   },
			   {
				path: 'orders', // <= Page URL
				component: OrdersComponent // <= Page component registration
			   },
			   {
				path: 'orders/create', // <= Page URL
				component: OrdersCreateComponent // <= Page component registration
			   },
			   {
				path: 'orders/edit/:id', // <= Page URL
				component: OrdersEditComponent // <= Page component registration
			   },
			   {
				path: 'orders/view/:id', // <= Page URL
				component: OrdersViewComponent // <= Page component registration
			   },
			   {
				path: 'guardians', // <= Page URL
				component: GurdiansComponent // <= Page component registration
			   },
			   {
				path: 'guardians/create', // <= Page URL
				component: GuardCreateComponent // <= Page component registration
		   	},
			   {
				path: 'guardians/edit/:id', // <= Page URL
				component: GuardEditComponent // <= Page component registration
			   },
			   {
				path: 'guardians/view/:id', // <= Page URL
				component: GuardViewComponent // <= Page component registration
		   	},

			   {
				path: 'otherguardian', // <= Page URL
				component: OtherguardianComponent // <= Page component registration
			   },
			   {
				path: 'otherguardian/create/:id', // <= Page URL
				component: OgCreateComponent // <= Page component registration
		   	},
			   {
				path: 'otherguardian/edit/:id', // <= Page URL
				component: OgEditComponent // <= Page component registration
			   },
			   {
				path: 'otherguardian/view/:id', // <= Page URL
				component: OgViewComponent // <= Page component registration
			   },
			   
			   {
				path: 'news', // <= Page URL
				component: NewsComponent // <= Page component registration
			   },
			   {
				path: 'news/create', // <= Page URL
				component: AddNewsComponent // <= Page component registration
		   	},
			   {
				path: 'news/edit/:id', // <= Page URL
				component: EditNewsComponent // <= Page component registration
			   },

			   {
				path: 'galleries', // <= Page URL
				component: GalleriesComponent // <= Page component registration
			   },
                {
				path: 'albums', // <= Page URL
				component: AlbumsComponent // <= Page component registration
			   },
               
			   {
				path: 'albums/create', // <= Page URL
				component: AddAlbumComponent // <= Page component registration
		   	},
			   {
				path: 'albums/edit/:id', // <= Page URL
				component: EditAlbumComponent // <= Page component registration
			   },
             {
				path: 'galleries/create', // <= Page URL
				component: AddGalleryComponent // <= Page component registration
		   	},
			   {
				path: 'galleries/edit/:id', // <= Page URL
				component: EditGalleryComponent // <= Page component registration
			   },

			   {
				path: 'kids', // <= Page URL
				component: KidsComponent // <= Page component registration
			   },
			   {
				path: 'kids/create/:id', // <= Page URL
				component: KidCreateComponent // <= Page component registration
		   	},
			   {
				path: 'kids/edit/:id', // <= Page URL
				component: KidEditComponent // <= Page component registration
			   },
			   {
				path: 'kids/view/:id', // <= Page URL
				component: KidViewComponent // <= Page component registration
		   	},
			   {
				path: 'booking', // <= Page URL
				component: BookingComponent // <= Page component registration
			   },
			   {
				path: 'booking/create/:id', // <= Page URL
				component: BookingCreateComponent // <= Page component registration
			   },
			   {
				path: 'booking/edit/:id', // <= Page URL
				component: BookingEditComponent // <= Page component registration
			   },

			   {
				path: 'teams', // <= Page URL
				component: TeamsComponent // <= Page component registration
				},

				{
					path: 'teams/reservation/:id', // <= Page URL
					component: ReservationComponent // <= Page component registration
				},
			   {
				path: 'admins', // <= Page URL
				component: AdminsComponent // <= Page component registration
			   },
			   {
				path: 'admins/create', // <= Page URL
				component: AdminCreateComponent // <= Page component registration
			   },
			   {
				path: 'admins/edit/:id', // <= Page URL
				component: AdminEditComponent // <= Page component registration
				},
				
//reports
				{
					path: 'eventreport', // <= Page URL
					component: EventsreportComponent // <= Page component registration
				},
				{
					path: 'teamreport', // <= Page URL
					component: TeamsreportComponent // <= Page component registration
				},
				{
					path: 'teamreport/details/:id', // <= Page URL
					component: TeamsDetailsComponent // <= Page component registration
					},
				{
					path: 'cabins-report', // <= Page URL
					component: CabinsReportsComponent // <= Page component registration
				},
				{
					path: 'cabins-report/details/:id', // <= Page URL
					component: CabinDetailsComponent // <= Page component registration
				},

				{
					path: 'payment-report', // <= Page URL
					component: PaymentComponent // <= Page component registration
				},
				{
					path: 'bookstatus-report', // <= Page URL
					component: BookingreportComponent // <= Page component registration
				},
				{
					path: 'gender-report', // <= Page URL
					component: GenderComponent // <= Page component registration
				},

			{
				path: 'mail',
				loadChildren: 'app/views/pages/apps/mail/mail.module#MailModule'
			},
			{
				path: 'ecommerce',
				loadChildren: 'app/views/pages/apps/e-commerce/e-commerce.module#ECommerceModule',
				// canActivate: [NgxPermissionsGuard],
				// data: {
				//  	permissions: {
				//  		only: ['accessToECommerceModule'],
				//  		redirectTo: 'error/403'
				// 	}
				// }
			},
			{
				path: 'ngbootstrap',
				loadChildren: 'app/views/pages/ngbootstrap/ngbootstrap.module#NgbootstrapModule'
			},
			{
				path: 'material',
				loadChildren: 'app/views/pages/material/material.module#MaterialModule'
			},
			{
				path: 'user-management',
				loadChildren: 'app/views/pages/user-management/user-management.module#UserManagementModule'
			},
			{
				path: 'builder',
				loadChildren: 'app/views/themes/default/content/builder/builder.module#BuilderModule'
			},
			{
				path: 'error/403',
				component: ErrorPageComponent,
				data: {
					'type': 'error-v6',
					'code': 403,
					'title': '403... Access forbidden',
					'desc': 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator'
				}
			},
			{path: 'error/:type', component: ErrorPageComponent},
			{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
			{path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
