// Angular
import { Component, Input, OnInit } from '@angular/core';

export interface Timeline2Data {
	time: string;
	text: string;
	icon?: string;
	attachment?: string;
}

@Component({
	selector: 'kt-timeline2',
	templateUrl: './timeline2.component.html',
	styleUrls: ['./timeline2.component.scss']
})
export class Timeline2Component implements OnInit {
	// Public properties
	@Input() data: Timeline2Data[];

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if (!this.data) {
			this.data = [
				{
					time: 'June 18 (10:00)',
					icon: 'fa fa-genderless kt-font-danger',
					text: 'Dina Wagdy Paid Cash',
				},
				{
					time: 'June 17 (12:45)',
					icon: 'fa fa-genderless kt-font-success',
					text: 'Michael Magdy Paid online - Waiting list',

				},
				{
					time: 'June 16 (14:00)',
					icon: 'fa fa-genderless kt-font-brand',
					text: 'Dina Wagdy Paid Cash',
				},
				{
					time: 'June 15 (17:00)',
					icon: 'fa fa-genderless kt-font-info',
					text: 'Dina Wagdy Paid Cash',
				},
				{
					time: 'June 14 (16:00)',
					icon: 'fa fa-genderless kt-font-brand',
					text: 'Dina Wagdy Paid Cash _ Waiting List',
						
				},
				{
					time: 'June 13 (17:00)',
					icon: 'fa fa-genderless kt-font-danger',
					text: 'Dina Wagdy Paid Online',
				},
			];
		}
	}
}
