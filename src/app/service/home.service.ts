import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Kids } from  '../kids';
import { Booking } from  '../booking';
import { Guardians } from  '../guardians';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  server:string;
  headers :Headers = new Headers();
  options:any;

  constructor(private httpClient: HttpClient, private servic:ConstantsService) {
    this.headers.append('enctype','multipart/form-data');
    this.headers.append('Content-Type','application/json');
    this.headers.append('X-Requested-With','XMLHttpRequest');

    this.options = new RequestOptions({headers:this.headers});
    this.server = this.servic.baseAppUrl+"home.php";
  }

   
    bookingData(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=bookingData&event_id='+id);
    }

    getGeneralStats(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=getGeneralStats&event_id='+id);
    }
    

    lastBooking(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=lastBooking&event_id='+id);
    }


    latestPayments(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=latestPayments&event_id='+id);
    }

    campersData(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=campersData&event_id='+id);
    }

    sports(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=sports&event_id='+id);
    }

    medicalConditions(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=medicalConditions&event_id='+id);
    }

    meetingPoints(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=meetingPoints&event_id='+id);
    }

    tshirts(id:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=tshirts&event_id='+id);
    }

    getYears():Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=getYears');
    }

    getCamps(year:number):Observable<Booking[]>{
      return this.httpClient.get<Booking[]>(this.server+'?do=getCamps&year='+year);
    }
    getnextCamp(){
      return this.httpClient.get<any>(this.server+'?do=getnextcamp');
    }

    
}
