import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { Points } from  '../meetingpoints';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class MeetingpointsService {

  server:string;
  headers :Headers = new Headers();
  options:any;
  results:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');
      // this.headers.append('Access-Control-Allow-Origin','*');
  
      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"meetingpoints.php";
  
    }
  
      list():Observable<Points[]>{
          return this.httpClient.get<Points[]>(this.server+'?do=list');
      }
  
      get(id:number):Observable<Points[]>{
        return this.httpClient.get<Points[]>(this.server+'?do=get&id='+id);
      }
  
  
      create(obj:any): Observable<Points> {
        return this.httpClient.post<Points>(this.server+'?do=create', obj);
      }
  
      edit(id,obj): Observable<Points> {
        return this.httpClient.post<Points>(this.server+'?do=edit&id='+id, obj);
     }
  
     delete(id:number): Observable<Points> {
      return this.httpClient.get<Points>(this.server+'?do=delete&id='+id);
   }
  
  }
  