import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Otherguardians } from  '../otherguardians';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class OtherguardiansService {

  server:string;
  headers :Headers = new Headers();
  options:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');

      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"otherguardians.php";
    }
  
    list():Observable<Otherguardians[]>{
      return this.httpClient.get<Otherguardians[]>(this.server+'?do=list');
  }

  get(id:number):Observable<Otherguardians[]>{
    return this.httpClient.get<Otherguardians[]>(this.server+'?do=get&id='+id);
  }

  create(obj): Observable<Otherguardians> {
    return this.httpClient.post<Otherguardians>(this.server+'?do=create', obj);
  }

  edit(id,obj): Observable<Otherguardians> {
    return this.httpClient.post<Otherguardians>(this.server+'?do=edit&id='+id, obj);
 }

 delete(id:number): Observable<Otherguardians> {
  return this.httpClient.get<Otherguardians>(this.server+'?do=delete&id='+id);
}

}
