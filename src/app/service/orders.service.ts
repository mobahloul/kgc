import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { Orders } from  '../orders';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

server:string;
headers :Headers = new Headers();
options:any;
results:any;

  constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
    this.headers.append('enctype','multipart/form-data');
    this.headers.append('Content-Type','application/json');
    this.headers.append('X-Requested-With','XMLHttpRequest');
    // this.headers.append('Access-Control-Allow-Origin','*');

    this.options = new RequestOptions({headers:this.headers});
    this.server = this.servic.baseAppUrl+"orders.php";
  }

    list():Observable<Orders[]>{
        return this.httpClient.get<Orders[]>(this.server+'?do=list');
    }

   get(id:number):Observable<Orders[]>{
      return this.httpClient.get<Orders[]>(this.server+'?do=get&id='+id);
    }


    create (obj:any): Observable<Orders> {
      return this.httpClient.post<Orders>(this.server+'?do=create', obj);
    }

    edit(id,obj): Observable<Orders> {
      return this.httpClient.post<Orders>(this.server+'?do=edit&id='+id, obj);
   }

   delete(id:number): Observable<Orders> {
    return this.httpClient.get<Orders>(this.server+'?do=delete&id='+id);
 }


}
