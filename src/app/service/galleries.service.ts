import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Galleries } from  '../galleries';
import { HttpClient,HttpEventType  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class GalleriesService {

  server:string;
  headers :Headers = new Headers();
  options:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');

      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"galleries.php";
    }
  
    list():Observable<Galleries[]>{
      return this.httpClient.get<Galleries[]>(this.server+'?do=list');
  }

  get(id:number):Observable<any>{
    return this.httpClient.get<any>(this.server+'?do=get&id='+id);
  }

  create(obj): Observable<any> {
    return this.httpClient.post<any>(this.server+'?do=create', obj,{
    reportProgress: true,
    observe: 'events'   
});
  }

  edit(id,obj): Observable<any> {
    return this.httpClient.post<any>(this.server+'?do=edit&id='+id, obj,{
    reportProgress: true,
    observe: 'events'   
});
 }

 delete(id:number): Observable<Galleries> {
  return this.httpClient.get<Galleries>(this.server+'?do=delete&id='+id);
}

}
