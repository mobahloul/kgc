
import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { Coupons } from  '../coupons';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class CouponsService {

server:string;

headers :Headers = new Headers();
options:any;
results:any;

  constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
    this.headers.append('enctype','multipart/form-data');
    this.headers.append('Content-Type','application/json');
    this.headers.append('X-Requested-With','XMLHttpRequest');
    // this.headers.append('Access-Control-Allow-Origin','*');

    this.options = new RequestOptions({headers:this.headers});
    this.server = this.servic.baseAppUrl+"coupon.php";
  }

    list():Observable<Coupons[]>{
        return this.httpClient.get<Coupons[]>(this.server+'?do=list');
    }

    get(id:number):Observable<Coupons[]>{
      return this.httpClient.get<Coupons[]>(this.server+'?do=get&id='+id);
    }


    create (obj:any): Observable<Coupons> {
      return this.httpClient.post<Coupons>(this.server+'?do=create', obj);
    }

    edit(id,obj): Observable<Coupons> {
      return this.httpClient.post<Coupons>(this.server+'?do=edit&id='+id, obj);
   }

   delete(id:number): Observable<Coupons> {
    return this.httpClient.get<Coupons>(this.server+'?do=delete&id='+id);
 }


 report():Observable<Coupons[]>{
  return this.httpClient.get<Coupons[]>(this.server+'?do=report');
}

}
