import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  readonly baseAppUrl: string = 'https://kgcdash.kidsgamescamp.com/api/';
  readonly paymentStatus = ['Show All', 'Unpaid', 'Paid', 'Waiting Paid', 'Waiting Unpaid', 'Canceled', 'Refunded'];
  readonly paymentMethod = ['Show All', 'Cash', 'Online'];
  readonly options = ['Show All', 'Yes', 'No'];
  readonly teams = ['Show All', 'Alpha', 'Omega'];
  readonly genders = ['Show All', 'Male', 'Female'];
  readonly relations = ['Show All', 'Father', 'Mother', 'Grandfather', 'Grandmother', 'Uncle', 'Aunt'];
  readonly tshirts = ['Show All', '8', '10', '12', '14', '16', 'S', 'M', 'L', 'Xs', 'XL', 'XXL'];
  readonly uploadsLink: string = 'https://kgc.apikora.com/uploads/galleries/';
  constructor() { }
}