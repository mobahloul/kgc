import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { Store } from  '../store';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  server:string;

  headers :Headers = new Headers();
  options:any;
  results:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');
      // this.headers.append('Access-Control-Allow-Origin','*');
  
      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"store.php";
    }
  
      list():Observable<Store[]>{
          return this.httpClient.get<Store[]>(this.server+'?do=list');
      }
  
      get(id:number):Observable<Store[]>{
        return this.httpClient.get<Store[]>(this.server+'?do=get&id='+id);
      }
  
  
      create (obj:any): Observable<Store> {
        return this.httpClient.post<Store>(this.server+'?do=create', obj);
      }
  
      edit(id,obj): Observable<Store> {
        return this.httpClient.post<Store>(this.server+'?do=edit&id='+id, obj);
     }
  
     delete(id:number): Observable<Store> {
      return this.httpClient.get<Store>(this.server+'?do=delete&id='+id);
   }
  
  
   report():Observable<Store[]>{
    return this.httpClient.get<Store[]>(this.server+'?do=report');
  }
  
  }
  