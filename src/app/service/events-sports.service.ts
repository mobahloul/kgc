import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { events } from  '../events';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class EventsSportsService {

server:string;
headers :Headers = new Headers();
options:any;
results:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) {
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');
      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"events_sports.php";
    }
  
    list():Observable<any>{
      return this.httpClient.get(this.server+'?do=list');
  }

 get(id:number):Observable<any>{
    return this.httpClient.get(this.server+'?do=get&id='+id);
  }


  create (obj:any,event_id:number): Observable<any> {
    return this.httpClient.post(this.server+'?do=create&id='+event_id, obj);
  }

  edit(id,obj): Observable<any> {
    return this.httpClient.post(this.server+'?do=edit&id='+id, obj);
 }

 delete(id:number): Observable<any> {
  return this.httpClient.get(this.server+'?do=delete&id='+id);
}


}
