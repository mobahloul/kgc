import { TestBed } from '@angular/core/testing';

import { OtherguardiansService } from './otherguardians.service';

describe('OtherguardiansService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OtherguardiansService = TestBed.get(OtherguardiansService);
    expect(service).toBeTruthy();
  });
});
