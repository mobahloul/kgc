import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { Sports } from  '../sports';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class SportsService {

server:string;

headers :Headers = new Headers();
options:any;
results:any;

  constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
    this.headers.append('enctype','multipart/form-data');
    this.headers.append('Content-Type','application/json');
    this.headers.append('X-Requested-With','XMLHttpRequest');
    // this.headers.append('Access-Control-Allow-Origin','*');

    this.options = new RequestOptions({headers:this.headers});
    this.server = this.servic.baseAppUrl+"sports.php";
  }

    list():Observable<Sports[]>{
        return this.httpClient.get<Sports[]>(this.server+'?do=list');
    }

    get(id:number):Observable<Sports[]>{
      return this.httpClient.get<Sports[]>(this.server+'?do=get&id='+id);
    }


    create (obj:any): Observable<Sports> {
      return this.httpClient.post<Sports>(this.server+'?do=create', obj);
    }

    edit(id,obj): Observable<Sports> {
      return this.httpClient.post<Sports>(this.server+'?do=edit&id='+id, obj);
   }

   delete(id:number): Observable<Sports> {
    return this.httpClient.get<Sports>(this.server+'?do=delete&id='+id);
 }


 report():Observable<Sports[]>{
  return this.httpClient.get<Sports[]>(this.server+'?do=report');
}

}
