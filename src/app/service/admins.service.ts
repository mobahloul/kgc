import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Admins } from  '../admins';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class AdminsService {

  server:string;
  headers :Headers = new Headers();
  options:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');
  
      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"admins.php";
    }
  
      list():Observable<Admins[]>{
          return this.httpClient.get<Admins[]>(this.server+'?do=list');
      }
  
      get(id:number):Observable<Admins[]>{
        return this.httpClient.get<Admins[]>(this.server+'?do=get&id='+id);
      }
  
  
      create (obj:any): Observable<Admins> {
        return this.httpClient.post<Admins>(this.server+'?do=create', obj);
      }
  
      edit(id,obj): Observable<Admins> {
        return this.httpClient.post<Admins>(this.server+'?do=edit&id='+id, obj);
     }

     pwdUpdate(id,obj): Observable<Admins> {
      return this.httpClient.post<Admins>(this.server+'?do=updatePassword&id='+id, obj);
    }
  
     delete(id:number): Observable<Admins> {
      return this.httpClient.get<Admins>(this.server+'?do=delete&id='+id);
   }
  
  }
  