import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Booking } from '../booking';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  server: string;

  headers: Headers = new Headers();
  options: any;

  constructor(public httpClient: HttpClient, public servic: ConstantsService) {
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('X-Requested-With', 'XMLHttpRequest');

    this.options = new RequestOptions({ headers: this.headers });
    this.server = this.servic.baseAppUrl + "booking.php";
  }

  list(): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=list');
  }

  listFamily(): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=listFamily');
  }

  get(id: number): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=get&id=' + id);
  }

  getByEvent(id: number, paid_type): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=getByEvent&paid_type=' + paid_type + '&id=' + id);
  }

  getFamily(id: number): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=getFamily&id=' + id);
  }

  getCamps(id: number): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=getCamps&id=' + id);
  }

  create(id, obj: any): Observable<Booking> {
    return this.httpClient.post<Booking>(this.server + '?do=create&id=' + id, obj);
  }

  edit(id, obj): Observable<Booking> {
    return this.httpClient.post<Booking>(this.server + '?do=edit&id=' + id, obj);
  }
  
  SendMails(id, obj: any): Observable<Booking> {
    return this.httpClient.post<Booking>(this.server + '?do=SendNotification&id=' + id, obj);
  }
  editFamily(id, obj): Observable<Booking> {
    return this.httpClient.post<Booking>(this.server + '?do=editFamily&id=' + id, obj);
  }

  delete(id: number): Observable<Booking> {
    return this.httpClient.get<Booking>(this.server + '?do=delete&id=' + id);
  }

  bookingReport(): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=bookingReport');
  }

  paymentReport(): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.server + '?do=paymentReport');
  }
  getTshirts() {
    return this.httpClient.get<any>(this.server + '?do=getTShirts');
  }
  getSports() {
    return this.httpClient.get<any>(this.server + '?do=getSports');
  }



}
