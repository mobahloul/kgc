import { TestBed } from '@angular/core/testing';

import { EventsMeetingpointsService } from './events-meetingpoints.service';

describe('EventsMeetingpointsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventsMeetingpointsService = TestBed.get(EventsMeetingpointsService);
    expect(service).toBeTruthy();
  });
});
