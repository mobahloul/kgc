import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Kids } from  '../kids';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class KidsService {

  server:string;
  headers :Headers = new Headers();
  options:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');
  
      this.options = new RequestOptions({headers:this.headers});

      this.server = this.servic.baseAppUrl+"Kids.php";
    }
  
      list():Observable<Kids[]>{
          return this.httpClient.get<Kids[]>(this.server+'?do=list');
      }
  
      get(id:number):Observable<Kids[]>{
        return this.httpClient.get<Kids[]>(this.server+'?do=get&id='+id);
      }
  
  
      create (id,obj) {
        return this.httpClient.post(this.server+'?do=create&id='+id, obj);
      }
  
      edit(id,obj): Observable<Kids> {
        return this.httpClient.post<Kids>(this.server+'?do=edit&id='+id, obj);
     }
  
     delete(id:number): Observable<Kids> {
      return this.httpClient.get<Kids>(this.server+'?do=delete&id='+id);
   }
  
  }
  