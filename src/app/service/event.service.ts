import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { events } from  '../events';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  server:string;
headers :Headers = new Headers();
options:any;
results:any;

  constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
    this.headers.append('enctype','multipart/form-data');
    this.headers.append('Content-Type','application/json');
    this.headers.append('X-Requested-With','XMLHttpRequest');
    // this.headers.append('Access-Control-Allow-Origin','*');

    this.options = new RequestOptions({headers:this.headers});
    this.server = this.servic.baseAppUrl+"events.php";
  }

    list():Observable<events[]>{
        return this.httpClient.get<events[]>(this.server+'?do=list');
    }

   get(id:number):Observable<events[]>{
      return this.httpClient.get<events[]>(this.server+'?do=get&id='+id);
    }


    create (obj:any): Observable<events> {
      return this.httpClient.post<events>(this.server+'?do=create', obj);
    }

    edit(id,obj): Observable<events> {
      return this.httpClient.post<events>(this.server+'?do=edit&id='+id, obj);
   }

   listFamily():Observable<events[]>{
    return this.httpClient.get<events[]>(this.server+'?do=listFamily');
}

   createFamily (obj:any): Observable<events> {
    return this.httpClient.post<events>(this.server+'?do=createFamily', obj);
  }

  editFamily(id,obj): Observable<events> {
    return this.httpClient.post<events>(this.server+'?do=editFamily&id='+id, obj);
 }

   delete(id:number): Observable<events> {
    return this.httpClient.get<events>(this.server+'?do=delete&id='+id);
 }


 report():Observable<events[]>{
  return this.httpClient.get<events[]>(this.server+'?do=report');
}

}
