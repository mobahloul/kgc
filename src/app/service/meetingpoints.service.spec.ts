import { TestBed } from '@angular/core/testing';

import { MeetingpointsService } from './meetingpoints.service';

describe('MeetingpointsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MeetingpointsService = TestBed.get(MeetingpointsService);
    expect(service).toBeTruthy();
  });
});
