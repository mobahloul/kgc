import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Galleries_albums } from  '../Galleries_albums';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class GalleriesAlbumsService {

  server:string;
  headers :Headers = new Headers();
  options:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');

      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"albums.php";
    }
  
    list():Observable<Galleries_albums[]>{
      return this.httpClient.get<Galleries_albums[]>(this.server+'?do=list');
  }

  get(id:number):Observable<Galleries_albums>{
    return this.httpClient.get<Galleries_albums>(this.server+'?do=get&id='+id);
  }

  create(obj): Observable<Galleries_albums> {
    return this.httpClient.post<Galleries_albums>(this.server+'?do=create', obj);
  }

  edit(id,obj): Observable<Galleries_albums> {
    return this.httpClient.post<Galleries_albums>(this.server+'?do=edit&id='+id, obj);
 }

 delete(id:number): Observable<Galleries_albums> {
  return this.httpClient.get<Galleries_albums>(this.server+'?do=delete&id='+id);
}

}
