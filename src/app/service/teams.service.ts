import { Injectable } from '@angular/core';
import {  Headers, RequestOptions } from '@angular/http';
import { Teams } from  '../teams';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  server:string;
  headers :Headers = new Headers();
  options:any;
  results:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');
      // this.headers.append('Access-Control-Allow-Origin','*');
  
      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"teams.php";
    }
  
      list():Observable<Teams[]>{
          return this.httpClient.get<Teams[]>(this.server+'?do=list');
      }
  
      get(id:number):Observable<Teams[]>{
        return this.httpClient.get<Teams[]>(this.server+'?do=get&id='+id);
      }
  
  
      create (obj:any): Observable<Teams> {
        return this.httpClient.post<Teams>(this.server+'?do=create', obj);
      }
  
      edit(id,obj): Observable<Teams> {
        return this.httpClient.post<Teams>(this.server+'?do=edit&id='+id, obj);
     }
  
     delete(id:number): Observable<Teams> {
      return this.httpClient.get<Teams>(this.server+'?do=delete&id='+id);
   }
  
    report():Observable<Teams[]>{
      return this.httpClient.get<Teams[]>(this.server+'?do=report');
    }
    
    teamsDetails(id:number):Observable<Teams[]>{
      return this.httpClient.get<Teams[]>(this.server+'?do=teamsDetails&event_id='+id);
    }

    cabinsReport():Observable<Teams[]>{
      return this.httpClient.get<Teams[]>(this.server+'?do=cabinsReport');
    }
    
    cabinsReportDetails(id:number):Observable<Teams[]>{
      return this.httpClient.get<Teams[]>(this.server+'?do=cabinsReportDetails&event_id='+id);
    }
    
    // getteam(event,team,subteam):Observable<Teams[]>{
    //   return this.httpClient.get<Teams[]>(this.server+'?do=getteam&event_id='+event+'&team='+team+'&subteam='+subteam);
    // }

    getteams(event):Observable<Teams[]>{
      return this.httpClient.get<Teams[]>(this.server+'?do=getteams&event_id='+event);
    }

    getcabins(event):Observable<Teams[]>{
      return this.httpClient.get<Teams[]>(this.server+'?do=getcabins&event_id='+event);
    }

    genderReport():Observable<Teams[]>{
      return this.httpClient.get<Teams[]>(this.server+'?do=genderReport');
    }

    // getcabin(event,cabin):Observable<Teams[]>{
    //   return this.httpClient.get<Teams[]>(this.server+'?do=getcabin&event_id='+event+'&cabin='+cabin);
    // }

  }
  