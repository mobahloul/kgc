import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Guardians } from  '../guardians';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class GuardiansService {

  server:string;
  servercoupon:string;
  headers :Headers = new Headers();
  options:any;
  
    constructor(private httpClient: HttpClient, private servic:ConstantsService) { 
      this.headers.append('enctype','multipart/form-data');
      this.headers.append('Content-Type','application/json');
      this.headers.append('X-Requested-With','XMLHttpRequest');

      this.options = new RequestOptions({headers:this.headers});
      this.server = this.servic.baseAppUrl+"guardians.php";
      this.servercoupon = this.servic.baseAppUrl+"coupon.php";
    }
  
    list():Observable<Guardians[]>{
      return this.httpClient.get<Guardians[]>(this.server+'?do=list');
  }

  get(id:number):Observable<Guardians[]>{
    return this.httpClient.get<Guardians[]>(this.server+'?do=get&id='+id);
  }

  getKids(id:number):Observable<Guardians[]>{
    return this.httpClient.get<Guardians[]>(this.server+'?do=getKids&id='+id);
  }

  getOtherGuardian(id:number):Observable<Guardians[]>{
    return this.httpClient.get<Guardians[]>(this.server+'?do=getOtherGuardian&id='+id);
  }

  create(obj): Observable<Guardians> {
    return this.httpClient.post<Guardians>(this.server+'?do=create', obj);
  }

  saveComment(id,obj): Observable<Guardians> {
    return this.httpClient.post<Guardians>(this.server+'?do=saveComment&id='+id, obj);
  }
  saveCoupon(id,obj) {
    return this.httpClient.post<any>(this.servercoupon+'?do=saveCoupon&id='+id, obj);
  }
  
  edit(id,obj): Observable<Guardians> {
    return this.httpClient.post<Guardians>(this.server+'?do=edit&id='+id, obj);
 }


 pwdUpdate(id,obj): Observable<Guardians> {
  return this.httpClient.post<Guardians>(this.server+'?do=updatePassword&id='+id, obj);
}

 delete(id:number): Observable<Guardians> {
  return this.httpClient.get<Guardians>(this.server+'?do=delete&id='+id);
}

}
