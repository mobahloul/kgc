import { TestBed } from '@angular/core/testing';

import { EventsSportsService } from './events-sports.service';

describe('EventsSportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventsSportsService = TestBed.get(EventsSportsService);
    expect(service).toBeTruthy();
  });
});
