export  class  events {
     id:null;
    name:  string;
    description:  string;
    startDate:  Date;
    endDate:  Date;
    ageFrom:  Date;
    ageTo:  Date;
    publishDate:  Date;
    openingDate:  Date;
    closingDate:  Date;
    boys:  number;
    girls:  number;
    boysCabins:  number;
    girlsCabins:  number;
    fees:  number;
}