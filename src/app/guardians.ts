export  class  Guardians {
   id:null;
   firstName:  string;
   middleName:  string;
   familyName:  string;
   gender:  string;
   email:  string;
   password:  string;
   mobile:  string;
   birthDate:  string;
   IDType:  string;
   IDNumber:  string;
   job:  string;
   employer:  string;
   relation:  string;
}