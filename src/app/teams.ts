export  class  Teams {
    id:null;
    event_id:  string;
    firstName:  string;
    middleName:  string;
    familyName:  string;
    kid_id:  string;
    team:  string;
    subteam:  string;
    cabin:  number;
    tshirt:  string;
    gender:  string;
    meetingPoint:  number;
}