export  class Store {
   id:null;
   productName:  string;
   productDescription:  string;
   price:  number;
   amount:  number;
   status:  string;
   image:  string;
}