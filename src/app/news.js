export  class News {
    id:number;
    title:  string;
    topic:  string;
    description:  string;
    image : string;
    user_id:  number;
    date : string;
 }