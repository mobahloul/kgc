import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({name: 'intpipe'})
export class Intpipe implements PipeTransform {

  transform(strval: string): number {
  
 
  return parseInt(strval);
  }
}