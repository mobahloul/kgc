import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({name: 'PostDate'})
export class PostDate implements PipeTransform {

  transform(date: string): string {
  
 
  return moment(date).format('D, MMMM YYYY');
  }
}