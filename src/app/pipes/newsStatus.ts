import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'newsStatus'})
export class newsStatus implements PipeTransform {
  output :string = '';
  transform(status: number): string {
  
  if(status == 0){
            this.output =  'Draft';
  
  }else if(status == 1){
  
   this.output = 'Published';
  }
  else{
       this.output = '0';
  }
  return this.output;
  }
}