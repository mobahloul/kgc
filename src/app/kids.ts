export  class  Kids {
   id:null;
   firstName:  string;
   middleName:  string;
   familyName: string;
   gender:  string;
   birthDate:  string;
   email:  string;
   mobile:  string;
   landline:  string;
   address:  string;
   school:  string;
   grade:  string;
   birthCertificate:  string;
   nationalID:  string;
   emergeContatNumber:  string;
   sports:  string;
   medicalIssues:  string;
   foodAllergy:  string;
   ongoingMedication:  string;
}